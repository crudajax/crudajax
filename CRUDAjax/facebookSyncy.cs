﻿using Facebook;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace CRUDAjax
{
     class facebookSyncy: System.Web.UI.Page
    {
        public void post2Face(string pgID, string msg, string AppID, string AppSec, string AccToken)
        {
            FacebookClient clientX = checkAuthorization(AppID, AppSec, AccToken);
            clientX.Post("/" + pgID + "/feed", new { message = msg });
            //clientX.Post("/460590547760073/feed", new { message = "hello from CRUD 2" });
        }
        public FacebookClient checkAuthorization(string appID, string appSec, string accToken)
        {
            string app_id = appID;
            string app_secret = appSec;
            string scope = "publish_to_groups, publish_pages, manage_pages";
            FacebookClient client = new FacebookClient();
            if (Request["code"] == null)
            {
                Response.Redirect(string.Format("https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope={2}",
                    app_id, Request.Url.AbsoluteUri, scope
                    ));
            }
            else
            {
                //Dictionary<string, string> tokens = new Dictionary<string, string>();
                string url = string.Format("https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&scope={2}&code={3}&client_secret={4}",
                    app_id, Request.Url.AbsoluteUri, scope, Request["code"].ToString(), app_secret
                    );
                //HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                //{
                //    StreamReader reader = new StreamReader(response.GetResponseStream());
                //    string vals = reader.ReadToEnd();
                //    foreach (string token in vals.Split(','))
                //    {

                //        int index = token.IndexOf(":");
                //        string str1 = token.Substring(2,index -3);
                //        string str2 = token.Substring(token.IndexOf(":")+2, token.Length - token.IndexOf(":")-3);

                //        tokens.Add(str1, str2);

                //    }
                //}
                //string access_token = tokens["access_token"];
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                string access_token = accToken;

                client = new FacebookClient(access_token);
            }
            return client;
        }
    }
}