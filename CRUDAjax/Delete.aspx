﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="CRUDAjax.Delete" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .cont {
            margin-top: 90px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cont">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Update Profile</h4>
                                <p class="card-category">Update your profile</p>
                            </div>
                            <div class="card-body">
                                <panel>
                                    <input type="hidden" id="txtID" />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="email" id="txtMail" class="form-control" placeholder="Email address" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" id="txtFname" class="form-control" placeholder="Fist Name"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" id="txtLname" class="form-control" placeholder="Last Name"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" id="txtAddress" class="form-control" placeholder="Adress"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" id="txtCity" class="form-control" placeholder="City" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>About You</label>
                                                <div class="form-group">
                                                    <textarea class="form-control" id="txtAbout" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" id="btnDelete" class="btn btn-primary pull-right">Delete</button>
                                    <div class="clearfix"></div>
                                </panel>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-profile">
                            <div class="card-avatar">
                                <a href="#pablo">
                                    <img class="img" src="assets/img/faces/marc.jpg" />
                                </a>
                            </div>
                            <div class="card-body">
                                <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                                <h4 class="card-title">Alec Thompson</h4>
                                <p class="card-description">
                                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                                </p>
                                <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


        <script type="text/javascript">
        $(document).ready(function () {

            var name = GetParameterValues('Name');
            var id = GetParameterValues('ID');
            //alert("Hello " + name + " your ID is " + id);
            function GetParameterValues(param) {
                var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < url.length; i++) {
                    var urlparam = url[i].split('=');
                    if (urlparam[0] == param) {
                        return urlparam[1];
                    }
                }
            }

            var id = GetParameterValues('ID');
            //alert(id);
            $.ajax({
                url: 'Service.asmx/getEmp',
                method: 'post',
                data: { ID: id },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    $("#txtID").val(jqueryXML.find('ID').text());
                    $("#txtMail").val(jqueryXML.find('email').text());
                    $("#txtFname").val(jqueryXML.find('fName').text());
                    $("#txtLname").val(jqueryXML.find('lName').text());
                    $("#txtAddress").val(jqueryXML.find('address').text());
                    $("#txtCity").val(jqueryXML.find('city').text());
                    $("#txtAbout").val(jqueryXML.find('about').text());
                },
                error: function (err) {
                    alert("error in retreiving employee! Sorry");
                }
            });
            //--------------------------------------------------------------------------------
            $("#btnDelete").click(function () {
                alert("clicked");
                var ID = GetParameterValues('ID');
                alert(ID);

                $.ajax({
                    url: 'Service.asmx/deleteEmp',
                    method: 'POST',
                    contentType: 'application/json;charset-utf-8',
                    data: JSON.stringify({ idDel : ID }),
                    dataType: 'json',
                    success: function (data) {
                        alert("Deleted!");
                        response(data.d);

                    },
                    error: function (err) {
                        alert("error in Deleting employee! Sorry");
                    }
                });
                //alert(date);

                return false;
            });
        });
    </script>

</asp:Content>
