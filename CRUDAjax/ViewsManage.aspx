﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="ViewsManage.aspx.cs" Inherits="CRUDAjax.ViewsManage" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <%--<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" ></script>--%>
    <style>
        .cont {
            /*margin-top: 90px;*/
            margin: 90px 20px 0 20px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }
        body .myAlert {
            width: 80%;
            z-index: 8000;
            margin: auto;
            display: none;
        }
        .butn {
            background-color: #9c27b0;
            color: white;
            cursor: pointer;
            margin-left: 25px;
            margin: 15px 0;
            border-radius: 5px;
        }

        .tbls {
            margin-top: 25px;
        }

        span.ui-helper-hidden-accessible {
            display: none;
        }

        body .ui-autocomplete {
            background-color: white;
            list-style-type: none;
            border: 1px solid #9C27B0;
            margin-top: 40px;
            font-size: 14px;
            font-family: "Roboto", "Helvetica", "Arial", sans-serif;
        }

        .hei {
            height: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cont">
        <div class="alert alert-success alert-dismissible fade show myAlert success" role="alert">
            <strong>Good News!</strong> <span id="succMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger alert-dismissible fade show myAlert failed" role="alert">
            <strong>Sorry!</strong> <span id="failMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="row" style="margin-left: 0;">
            <input type="text" class="hei form-group col-sm-8 col-md-6" id="txtTblsAuto" placeholder="Table name" />
            <button type="button" id="btnTable" style="margin-top: 7px; height: 41px; margin-left: 15px;" class="hei butn btn-sm col-sm-4 col-md-3">Choose this table</button>

        </div>
        <div class="tbls">
        </div>

        <br />
        <div class="row" style="margin-left: 0;">
            <input class="hei form-group col-sm-8 col-md-3" id="txtViewName" placeholder=" View Name" />
            <button type="button" id="test" style="margin-top: 7px; height: 41px; margin-left: 15px;" class="hei butn btn-sm">Create View</button>
        </div>
    </div>


    <script src="multiselect/js/jquery.multi-select.js" type="text/javascript"></script>

    <%--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>--%>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtTblsAuto').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: 'Service.asmx/autoTbls',
                        method: 'POST',
                        contentType: 'application/json;charset-utf-8',
                        data: JSON.stringify({ prefixText: $('#txtTblsAuto').val(), count: 1 }),
                        dataType: 'json',
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (err) {
                            //alert("No table with this name, please make sure of the spelling!");
                            $('.myAlert.failed').css('display', 'block');
                            $('#failMsg').html('No table with this name, please make sure of the spelling!');
                        }
                    });
                }
            });


            var tblsSelected = new Array();
            var tblCols = new Array();
            var selectedAttr = new Array();
            var Alias = new Array();
            var Test = new Array();
            var elemNum = 0;

            $('#btnTable').click(function () {

                var tblNme = $('#txtTblsAuto').val();

                var Index = tblsSelected.indexOf(tblNme);
                if (Index !== -1) {
                    alert('This Table Is Already Exist');
                }
                else {
                    tblsSelected.push($('#txtTblsAuto').val());
                    elemNum++;

                    $.ajax({
                        url: 'Service.asmx/getTblCols',
                        method: 'post',
                        data: { tableName: tblNme },
                        dataType: 'xml',
                        success: function (data) {
                            var jqueryXML = $(data);
                            var tblsAttr = jqueryXML.find("COLUMN_NAME").length;

                            var options = "";
                            for (var i = 0; i < tblsAttr; i++) {
                                tblCols.push(tblNme + "." + jqueryXML.find("COLUMN_NAME").eq(i).text());
                                options += "<option value='" + jqueryXML.find("COLUMN_NAME").eq(i).text() + "'>" + jqueryXML.find("COLUMN_NAME").eq(i).text() + "</option>";
                            }
                            //alert(options);

                            //var inputText = '<input type="text" placeholder="on firstTable.Key = Second Table.Key" style="width:100%; margin-top:10px;">';

                            if (elemNum > 1) {
                                $('.tbls').append($('<div>').addClass('col-sm-12').css({ 'margin-bottom': '25px' }).append($('<label>').text(tblNme)).append(($('<select>').addClass('col-md-12').attr('multiple', 'multiple').attr('id', tblNme)).append(options)).append($('<input>').attr('placeholder', 'on firstTable.Key = Second Table.Key').css('width', '100%').css('margin-top', '10px').addClass('form-group').attr('type', 'text').attr('id', 'txtCond' + tblNme)));
                            }
                            else if (elemNum == 1) {
                                $('.tbls').append($('<div>').addClass('col-sm-12').css({ 'margin-bottom': '25px' }).append($('<label>').text(tblNme)).append(($('<select>').addClass('col-md-12').attr('multiple', 'multiple').attr('id', tblNme)).append(options)));
                            }


                            $('#' + tblNme).multiSelect({
                                afterSelect: function (values) {

                                    var value = values + "";
                                    var Index = Alias.indexOf(value);
                                    if (Index !== -1) {
                                        Alias.push(tblNme + "" + value);
                                        //alert(value);
                                        //alert('EXIST');
                                    }
                                    else {
                                        Alias.push(value);
                                        //alert(value);
                                        //alert('Not Exist');
                                    }

                                    selectedAttr.push(tblNme + "." + value);

                                },
                                afterDeselect: function (values) {
                                    var value = values + "";
                                    var Index = Alias.indexOf(value);
                                    if (Index !== -1) {
                                        Alias.pop(tblNme + "" + value);
                                        //alert(value);
                                        //alert('EXIST');
                                    }
                                    else {
                                        Alias.pop(value);
                                        //alert(value);
                                        //alert('Not Exist');
                                    }

                                    selectedAttr.pop(tblNme + "." + values);
                                }


                            });

                        },
                        error: function (err) {
                            //alert("error in retrieving Data! Sorry");
                            $('.myAlert.failed').css('display', 'block');
                            $('#failMsg').html('failed in retrieving data,please try again');
                        }
                    });
                }


            });

            $('#test').click(function () {
                var str = "";
                str += " SELECT " + selectedAttr.toString() + " ";
                var tables = " FROM " + tblsSelected[0] + " ";
                for (var i = 1; i < tblsSelected.length; i++) {
                    var selectorInp = "txtCond" + tblsSelected[i];
                    tables += " INNER JOIN " + tblsSelected[i] + " " + $("#" + selectorInp).val();
                    //alert($("#txtCond" + tblsSelected[i]).val());
                    //alert(selectorInp);
                    //alert(tables);
                }
                str += tables;
                var viewNam = $('#txtViewName').val();
                //alert(viewNam);
                $.ajax({
                    url: 'Service.asmx/createView',
                    method: 'POST',
                    contentType: 'application/json;charset-utf-8',
                    data: JSON.stringify({ viewName: viewNam, selectStr: str }),
                    dataType: 'json',
                    success: function (data) {
                        //alert("Edited!");
                        //alert(data.d);
                        //alert(str);
                        //alert(viewNam);
                        $('.myAlert.success').css('display', 'block');
                        $('#succMsg').html('data succefuly edited');
                        //alert("success");
                    },
                    error: function (err) {
                        //alert("error in Editing entity! Sorry");
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed to Edited data, please try again');
                    }
                });


                $.ajax({
                    url: 'Service.asmx/inserTblView',
                    method: 'POST',
                    contentType: 'application/json;charset-utf-8',
                    data: JSON.stringify({ viewName: viewNam }),
                    dataType: 'json',
                    success: function (data) {
                        //alert("success Insert In Tbl Views");
                        $('.myAlert.success').css('display', 'block');
                        $('#succMsg').html('success Insert In Tbl Views');
                    },
                    error: function (err) {
                        //alert(err.error);
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('error, please try again');
                    }
                });

                //alert(str);
                return false;

            });

        });
    </script>

</asp:Content>
