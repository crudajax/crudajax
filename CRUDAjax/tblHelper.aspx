﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="tblHelper.aspx.cs" Inherits="CRUDAjax.tblHelper" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Table Helper
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
    <%-- Paggination --%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <%--<link rel="stylesheet" href="assets/bootstrab/css/bootstrap.css" />--%>
    <link rel="stylesheet" href="assets/css/paging.css" />


    <script src="assets/bootstrab/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="assets/js/jquery.table.hpaging.js"></script>


    <%-- Paggination --%>


    

    <style>
        .cont {
            /*margin-top: 90px;*/
            margin: 90px 20px 0 20px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }

        body .fieldy {
            display: -webkit-inline-box;
        }
        /*.tab-pane {
            display:none;
        }
        .activeTab {
            display:block;
        }*/
        .form-group {
            width: 100%;
        }

        .navtabs {
            background-color: #9c27b0;
        }

            .navtabs li {
                padding: 8px 0;
            }

                .navtabs li a {
                    color: white !important;
                    font-weight: bold;
                    font-style: italic;
                }

        body .myAlert {
            width: 80%;
            z-index: 8000;
            margin: auto;
            display: none;
        }


        /*pagging*/
        .btn {
            background-color: #9c27b0;
            color: white;
        }

            .btn:hover {
                background-color: #9c27b0;
            }

        .txt {
            border-radius: 8px;
            padding: 5px;
        }

        .pagination .active {
            background-color: #9c27b0 !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            //Queries uses getUrlVars() function which is used to get query strings and call it as Queries["Query_String_Name"]
            var tblName = "tblHelper";
            loadRelation(tblName);
        });
        //to load relations >> get the column names from tblHelper and relations tables where there is a reference query

        function loadRelation(tblName) {
            var trial = "";
            var relatedTbls = [];
            var relatedColsID = [];
            var relatedColsName = [];
            //check if there is any related column in this table

            //refTable is the other table
            //refCol is the column in the other table
            //fromCol is the column in this table
            $.ajax({
                url: 'Service.asmx/relatedTbls',
                method: 'post',
                data: { tblName: tblName },
                dataType: 'xml',
                success: function (data) {
                    var jqueryTbls = $(data);
                    for (var i = 0; i < jqueryTbls.find('Table').length; i++) {
                        relatedTbls.push(jqueryTbls.find('Table').eq(i).find('refTbl').eq(0).text());
                        relatedColsID.push(jqueryTbls.find('Table').eq(i).find('refCol').eq(0).text());
                        relatedColsName.push(jqueryTbls.find('Table').eq(i).find('fromCol').eq(0).text());
                    }
                },
                error: function (err) {
                    alert("error in retrieving relation! Sorry");
                }
            });

            //get column names from tblhelper and add related column's query to it

            $.ajax({
                url: 'Service.asmx/colNames',
                method: 'post',
                data: { tblName: tblName },
                dataType: 'xml',
                success: function (data) {
                    for (var x = 0; x < relatedTbls.length; x++) {
                        var trial = "";
                        var jqueryHead = $(data);
                        for (var i = 0; i < jqueryHead.find(relatedTbls[x]).length; i++) {
                            var valRela = jqueryHead.find(relatedTbls[x]).eq(i).find("ID").eq(0).text();
                            if (relatedColsName[x].match("^access")) {
                                //bit values are 1 or 0 so we create a dropdown list to show yes/no instead
                                valRela = parseInt(jqueryHead.find(relatedTbls[x]).eq(i).find("bitValue").eq(0).text());
                            }
                            trial += '<option value="' + valRela + '">' + jqueryHead.find(relatedTbls[x]).eq(i).find(relatedColsID[x]).eq(0).text() + '</option>';
                        }
                        $('#relationHdn').append($('<select>').attr('id', 'sl' + relatedColsName[x]).html(trial));
                    }
                },
                error: function (err) {
                    alert("error in retrieving relation! Sorry");
                }

            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>

    <div class="cont">

        <%----------------------------------------------------- Alerts ------------------------------------------------%>
        <div class="alert alert-success alert-dismissible fade show myAlert success" role="alert">
            <strong>Good News!</strong> <span id="succMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger alert-dismissible fade show myAlert failed" role="alert">
            <strong>Sorry!</strong> <span id="failMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <%----------------------------------------------------- ------ ------------------------------------------------%>


        <ul class="navtabs nav nav-tabs">
            <%--            <li class="col-xl-6"><a id="inserty" data-toggle="tab" href="#insert">Insert</a></li>--%>
            <li class="col-xl-12"><a id="retrievy" data-toggle="tab" href="#retreivey">Retreive</a></li>
        </ul>

        <div class="tab-content">
            <div id="insert" class="tab-pane fade">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title"><span class="entitySpan">TblHelper</span> </h4>
                                        <p class="card-category">Complete your profile</p>
                                    </div>
                                    <div class="card-body">
                                        <panel class="frm">

                                            <input type="hidden" id="hiddenElm" />
                                            <div class="inputs"></div>
                                            <button type="submit" id="btnInsert" class="btn btn-primary pull-right" >Insert</button>
                                        </panel>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



            <div id="retreive" class="tab-pane fade active show">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title "><span class="entitySpan"></span>Table</h4>
                                        <p class="card-category">Here is a subtitle for this table</p>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <%-- Paginnation --%>
                                            <input id="pglmt" placeholder="Page Limit" title="Page Limit" class="col-xl-7 txt" />
                                            <button id="btnApply" class="btn col-xs-offset-1 col-xs-4">Apply</button>
                                            <%-- Paginnation --%>
                                            <table id="dataTable" class="table">
                                                <thead class=" text-primary">
                                                    <tr id="tableHeader">
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div id="relationHdn" style="display: none;"></div>
    </div>


    <%-- Paginnation --%>
    <script type="text/javascript">
        

        $(function () {
            $("#dataTable").hpaging({ "limit": 3 });
        });

        $("#btnApply").click(function (e) {
            var lmt = $("#pglmt").val();
            $("#dataTable").hpaging("newLimit", lmt);
            e.preventDefault();

        });
    </script>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36251023-1']);
        _gaq.push(['_setDomainName', 'jqueryscript.net']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <%-- Paginnation --%>


    <script type="text/javascript">
        $("#inserty").click(function () {
            //insertLoadFrm();
            $('.myAlert.failed').css('display', 'block');
            $('#failMsg').html('Sorry it is not allowed to insert from here , fields are genereated automatically');
        });

        //for the insert part of any data to table .. we need to create fields first
        function insertLoadFrm() {
            $('.inputs').html('');

            $.ajax({
                url: 'Service.asmx/getTblColsNew',
                method: 'post',
                data: { tableName: "tblHelper" },
                dataType: 'xml',
                success: function (data) {

                    //var tblCols = [];
                    var colDTypes = [];
                    var colWidth = [];
                    var colDataType = [];
                    var jqueryXMLHead = $(data);
                    //var KeyDHead = jqueryXMLHead.find('Table').find("TABLE_SCHEMA:empty").length;
                    var KeyDPH = jqueryXMLHead.find('Table').length;

                    var alCols = [];
                    for (var i = 0; i < parseInt(KeyDPH) ; i++) {
                        var fieldys = jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text();
                        if (fieldys == "textarea") {
                            var firstPart = '<div class="fieldy col-md-' + jqueryXMLHead.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="form-group"><label class="bmd-label-floating">';

                            var secondPart = '</label><textarea   class="form-control" colType="' + jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text() + '" for="' + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + '"  rows="5"></textarea></div></div>';

                            $('.inputs').append(firstPart + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + secondPart);
                            alCols.push(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text());
                        }
                        else if (fieldys == "image") {
                            //alert("DAKHAL rakam"+i);
                            var firstPart = '<div class="fieldy col-md-' + jqueryXMLHead.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="form-group"><label class="bmd-label-floating">';

                            var secondPart = '</label><input type="file"  style="width: 100% ; display: -webkit-inline-box;" class="form-control fileUp" colType="' + jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text() + '" for="' +
                                 jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + '"  /> <br />' +
                                 '</div></div>';
                            //var secondPart = '</label> <br />' +
                            //    '<input type="file"  style="width: 100% ; display: -webkit-inline-box;" class="form-control fileUp" colType="' + jqueryXML.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '" for="' +
                            //     jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '"  /> <br />' +
                            //     '<img src="uploads/' + textStr + '"  class="img-responsive" style="height:80px"/>' +
                            //     '</div></div></div>';

                            $('.inputs').append(firstPart + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + secondPart);
                            alCols.push(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text());
                        }

                        else if (fieldys == "select") {
                            var selectID = jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text();
                            //alert(selectID);
                            var list = $('#sl' + selectID).html();
                            var firstPart = '<div class="fieldy col-md-' + jqueryXMLHead.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="form-group"><label class="bmd-label-floating">';

                            var secondPart = '</label>' +
                                '<select   class="form-control" colType="' + jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text() + '" for="' + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + '">' +
                                //loadRelation(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text(), jqueryXMLHead.find('Table').eq(i).find("tblName").eq(0).text())
                                list
                                + '</select>' +
                                '</div></div>';

                            $('.inputs').append(firstPart + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + secondPart);
                            alCols.push(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text());
                        }
                        else {
                            var firstPart = '<div class="fieldy col-md-' + jqueryXMLHead.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="form-group"><label class="bmd-label-floating">';

                            var secondPart = '</label><input type="text" class="form-control" colType="' + jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text() + '" for="' + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + '"/></div></div>';

                            $('.inputs').append(firstPart + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + secondPart);
                            alCols.push(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text());
                        }
                    }
                    $("#hiddenElm").attr('value', alCols.toString());

                    var buttton = '<button type="submit" id="btnInsert" class="btn btn-primary pull-right" onclick="btnInsert">Insert</button><div class="clearfix"></div>';

                    //$('.frm').append(buttton);


                },
                error: function (err) {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving table header, please try again');
                }
            });
        }
        //function to upload files to server
        $("#btnInsert").click(function () {
            var allCols = $("#hiddenElm").val();
            var tblColsValues = [];
            var tblColType = [];

            var arrColName = allCols.split(','); // empty string separator

            for (var i = 0; i < arrColName.length; i++) {
                if ($("#txt" + arrColName[i]).attr('colType') != 'image') {
                    tblColType.push($("#txt" + arrColName[i]).attr('colType'));
                    tblColsValues.push($("#txt" + arrColName[i]).val());
                }
            }

            var str = "";
            for (var xz = 0; xz < arrColName.length; xz++) {
                if ($("#txt" + arrColName[xz]).attr('colType') == 'image') {
                    tblColType.push($("#txt" + arrColName[xz]).attr('colType'));
                }
            }
            for (var xyz = 0; xyz < count(tblColType) ; xyz++) {
                alert('has images becuse count is : ' + count(tblColType));
                var data = new FormData();
                var files = $(".fileUp").get(xyz).files;
                data.append("UploadedImage", files[xyz]);
                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: "Service.asmx/UploadFile",
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (data) {
                        var jqueryy = $(data);
                        var newName = $(jqueryy).find('string').text();
                        tblColsValues.push(newName);
                    }
                });
                if (xyz == count(tblColType) - 1) {

                    ajaxRequest.done(function (xhr, textStatus) {

                        for (var i = 0; i < tblColsValues.length ; i++) {
                            if (i == tblColsValues.length - 1) {
                                if (tblColType[i] == 'number') {
                                    str += tblColsValues[i];
                                }
                                else {
                                    str += "N'" + tblColsValues[i] + "'";
                                }

                            }
                            else {
                                if (tblColType[i] == 'number') {
                                    str += tblColsValues[i] + ",";
                                }
                                else {
                                    str += "N'" + tblColsValues[i] + "', ";
                                }
                            }

                        }
                        Queries = getUrlVars();
                        var tblNames = "tblHelper";

                        if (count(tblColType) > 0) {

                            var ajaxRequest2 = insertNewData(tblNames, allCols, str);
                        }
                    });
                }
            }
            for (var i = 0; i < tblColsValues.length ; i++) {
                if (i == tblColsValues.length - 1) {
                    if (tblColType[i] == 'number') {
                        str += tblColsValues[i];
                    }
                    else {
                        str += "N'" + tblColsValues[i] + "'";
                    }

                }
                else {
                    if (tblColType[i] == 'number') {
                        str += tblColsValues[i] + ",";
                    }
                    else {
                        str += "N'" + tblColsValues[i] + "', ";
                    }
                }

            }
            Queries = getUrlVars();
            var tblNames = "tblHelper";
            if (count(tblColType) == 0) {

                var ajaxRequest2 = insertNewData(tblNames, allCols, str);
            }




            return false;
        });
        //function to count number of fields that represents images in this table
        function count(myArrarr) {
            array_elements = myArrarr;


            var current = null;
            var cnt = 0;
            for (var i = 0; i < array_elements.length; i++) {
                if (array_elements[i] == 'image') {
                    cnt++;
                }
            }
            return cnt;

        }
        //function to insert data into database
        function insertNewData(tblNames, allCols, str) {
            //alert('before insert');
            //alert(tblNames); 
            //alert(allCols);
            //alert(str);
            $.ajax({
                beforeSend: function () {
                    //alert('start upload');
                    //uploadPic();
                },
                url: 'Service.asmx/inserGen',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: tblNames, cols: allCols, txt: str }),
                dataType: 'json',
                success: function (data) {
                    var newUrl = $(location).attr('href');
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('added succefuly to the ' + tblNames + 'table');
                    location.reload();

                },
                error: function (err) {
                    //alert("error in adding Entity! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to add data to the ' + tblNames + ' table, please try again');
                }
            });
        }

        //function to delete data
        //function deleteGen(IDDelete) {
        //    var tblNm = $(".entitySpan").eq(0).text();
        //    $.ajax({
        //        url: 'Service.asmx/deleteGen',
        //        method: 'POST',
        //        contentType: 'application/json;charset-utf-8',
        //        data: JSON.stringify({ tblName: tblNm, idDel: parseInt(IDDelete) }),
        //        dataType: 'json',
        //        success: function (data) {
        //            //return false;
        //            $('.myAlert.success').css('display', 'block');
        //            $('#succMsg').html('deleted succefuly');
        //        },
        //        error: function (err) {
        //            $('.myAlert.failed').css('display', 'block');
        //            $('#failMsg').html('failed to delete data, please try again');
        //        }
        //    });
        //    var form = $("#form1");
        //    var post_url = form.attr('action');
        //    var post_data = getGen();
        //    $.ajax({
        //        type: 'POST',
        //        url: post_url,
        //        data: post_data,
        //        success: function () {
        //            //return false;
        //        }
        //    });
        //    return false;
        //}

        //table data to be loaded after table header
        function getGen() {
            $('#dataTable tbody tr').remove();
            //tblHeader();
            $.ajax({

                url: 'Service.asmx/selectGen',
                method: 'post',
                data: { tblNameGen: "tblHelper" },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    //count of rows data
                    var KeyD = jqueryXML.find("Table").length;
                    //count of table columns
                    var countx = $(data).find("Table").eq(0).children().length;

                    for (var i = 0; i < parseInt(KeyD) ; i++) {
                        $('#dataTable tbody').append($('<tr>'));

                        var rowData = jqueryXML.find('Table').eq(i).children();

                        for (var x = 0; x < countx - 1 ; x++) {


                            var TAGname = rowData.eq(x + 1).get(0).tagName;
                            var TAGToLower = TAGname.toLowerCase();

                            if (TAGToLower.includes("url") == true || TAGToLower.includes("image") == true) {
                                //alert("beboo :" + countx);
                                var uploads = 'uploads/' + rowData.eq((x + 1)).text();
                                //alert(rowData.eq((x + 1)).text());
                                $('#dataTable tbody tr:nth-of-type(' + (i + 1) + ')')
                            .append($('<td>').append($('<img>').attr("src", uploads).addClass('img-responsive').css({ "width": "80px" })));
                            }
                            else {
                                //alert(TAGname);
                                $('#dataTable tbody tr:nth-of-type(' + (i + 1) + ')')
                            .append($('<td>').append(rowData.eq((x + 1)).text()));
                            }
                            //alert("x = " + x + " and i = " + i + " for tag is : " + TAGname);
                        }
                        //alert('edit ?');
                        $('#dataTable tbody tr:nth-of-type(' + (i + 1) + ')')
                            .append($('<td>').append($('<a>').attr('href', 'Update.aspx?ID=' + jqueryXML.find('ID').eq(i).text() + '&Entity=' + "tblHelper").append($('<i>').attr('class', 'material-icons').text('edit'))))
                            //.append($('<td>').append($('<div>').attr('ids', jqueryXML.find('ID').eq(i).text()).attr('class', 'btnDelete').attr('onclick', 'deleteGen(' + parseInt(jqueryXML.find("ID").eq(i).text()) + ');').append($('<i>').attr('class', 'fa fa-trash').attr('style', "font-size: 24px; color: red;     cursor: pointer;"))))
                            .append($('<input>').val(jqueryXML.find('ID').eq(i).text()).attr('type', 'hidden').addClass('idHidden'));
                    }

                },
                error: function (err) {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving Entity, please try again');
                }
            });

        }
        $(window).load(tblHeader());
        //table in crud needs the first table head row so we need to load the header titles which is an alternate title to the column real names
        function tblHeader() {
            $('#tableHeader').html('');
            //var Queries = getUrlVars();
            var ajaxHeaderReq =
                $.ajax({
                    url: 'Service.asmx/getTblColsNew',
                    method: 'post',
                    data: { tableName: "tblHelper" },
                    dataType: 'xml',
                    success: function (data) {
                        var jqueryXMLHead = $(data);
                        //alert(KeyDHead);
                        var KeyDHead = jqueryXMLHead.find("Table").length;
                        //alert(KeyDHead);
                        for (var i = 0; i < parseInt(KeyDHead) ; i++) {
                            $('#tableHeader')
                            //.append($('<th>').append(jqueryXMLHead.find('COLUMN_NAME').eq(i + 1).text()))
                            .append($('<th>').append(jqueryXMLHead.find('colAlias').eq(i).text()))
                        }
                        $('#tableHeader').append($('<th>').attr('colspan', '1').addClass('text-center').text('Edit'))

                    },
                    error: function (err) {
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed in retrieving table header, please try again');
                    }
                });
            //after we load table header we load the data of the table
            ajaxHeaderReq.done(
                getGen()
            );
        }
    </script>
</asp:Content>
