﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="app.aspx.cs" Inherits="CRUDAjax.app" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
    Manage Data
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .cont {
            /*margin-top: 90px;*/
            margin: 90px 20px 0 20px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }

        .disabled, .disabled > * {
            pointer-events: none;
            color: #808080 !important;
        }

        body .fieldy {
            display: -webkit-inline-box;
        }
        /*.tab-pane {
            display:none;
        }
        .activeTab {
            display:block;
        }*/
        .form-group {
            width: 100%;
        }

        .navtabs {
            background-color: #9c27b0;
        }

            .navtabs li {
                padding: 8px 0;
            }

                .navtabs li a {
                    color: white !important;
                    font-weight: bold;
                    font-style: italic;
                }

        body .myAlert {
            width: 80%;
            z-index: 8000;
            margin: auto;
            display: none;
        }
    </style>
    <script>
        $(window).load(function () {



            $.ajax({
                url: 'Service.asmx/checkLevel',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: Queries["Entity"] }),
                dataType: 'json',
                success: function (data) {
                    if (data.d == "Allowed") {

                    }
                    else {
                        //$('.myAlert.failed').css('display', 'block');
                        //$('#failMsg').html('you have no acess for this table');

                        $(location).attr("href", "Default.aspx");
                        //window.location = "Default.aspx";
                    }
                },
                error: function (err) {
                    //$('.myAlert.failed').css('display', 'block');
                    //$('#failMsg').html('failed to retrieve, please try again');
                }
            });
        });
    </script>
    <script>

        $(document).ready(function () {


            //Queries uses getUrlVars() function which is used to get query strings and call it as Queries["Query_String_Name"]
            var tblName = Queries["Entity"];
            loadRelation(tblName);
        });
        //to load relations >> get the column names from tblHelper and relations tables where there is a reference query

        function loadRelation(tblName) {
            var trial = "";
            var relatedTbls = [];
            var relatedColsID = [];
            var relatedColsName = [];
            //check if there is any related column in this table

            //refTable is the other table
            //refCol is the column in the other table
            //fromCol is the column in this table
            $.ajax({
                url: 'Service.asmx/relatedTbls',
                method: 'post',
                data: { tblName: tblName },
                dataType: 'xml',
                success: function (data) {
                    var jqueryTbls = $(data);
                    for (var i = 0; i < jqueryTbls.find('Table').length; i++) {
                        relatedTbls.push(jqueryTbls.find('Table').eq(i).find('refTbl').eq(0).text());
                        relatedColsID.push(jqueryTbls.find('Table').eq(i).find('refCol').eq(0).text());
                        relatedColsName.push(jqueryTbls.find('Table').eq(i).find('fromCol').eq(0).text());
                    }
                },
                error: function (err) {
                    //alert("error in retrieving relation! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving relation, please try again');
                }
            });

            //get column names from tblhelper and add related column's query to it

            $.ajax({
                url: 'Service.asmx/colNames',
                method: 'post',
                data: { tblName: tblName },
                dataType: 'xml',
                success: function (data) {
                    for (var x = 0; x < relatedTbls.length; x++) {
                        var trial = "";
                        var jqueryHead = $(data);
                        for (var i = 0; i < jqueryHead.find(relatedTbls[x]).length; i++) {
                            var valRela = jqueryHead.find(relatedTbls[x]).eq(i).find("ID").eq(0).text();
                            if (relatedColsName[x].match("^access")) {
                                //bit values are 1 or 0 so we create a dropdown list to show yes/no instead
                                valRela = parseInt(jqueryHead.find(relatedTbls[x]).eq(i).find("bitValue").eq(0).text());
                            }
                            trial += '<option value="' + valRela + '">' + jqueryHead.find(relatedTbls[x]).eq(i).find(relatedColsID[x]).eq(0).text() + '</option>';
                        }
                        $('#relationHdn').append($('<select>').attr('id', 'sl' + relatedColsName[x]).html(trial));
                    }
                },
                error: function (err) {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving relation, please try again');
                    //alert("error in retrieving relation! Sorry");
                }

            });

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cont">
        <%----------------------------------------------------- Alerts ------------------------------------------------%>
        <div class="alert alert-success alert-dismissible fade show myAlert success" role="alert">
            <strong>Good News!</strong> <span id="succMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger alert-dismissible fade show myAlert failed" role="alert">
            <strong>Sorry!</strong> <span id="failMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <%----------------------------------------------------- ------ ------------------------------------------------%>


        <ul class="navtabs nav nav-tabs">
            <li class="col-xl-6"><a id="inserty" data-toggle="tab" href="#insert">Insert</a></li>
            <li class="col-xl-6"><a id="retrievy" data-toggle="tab" href="#retreive">Retreive</a></li>
        </ul>

        <div class="tab-content">
            <div id="insert" class="tab-pane fade">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title"><span class="entitySpan"></span></h4>
                                        <p class="card-category">Complete your profile</p>
                                    </div>
                                    <div class="card-body">
                                        <panel class="frm">

                                            <input type="hidden" id="hiddenElm" />
                                            <div class="inputs">

                                            </div>
                                               <button type="submit" id="btnInsert" class="btn btn-primary pull-right" >Insert</button> 

                                        </panel>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div id="retreive" class="tab-pane fade active show">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title "><span class="entitySpan"></span>Table</h4>
                                        <p class="card-category">Here is a subtitle for this table</p>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="dataTable" class="table">
                                                <thead class=" text-primary">
                                                    <tr id="tableHeader">
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div id="relationHdn" style="display: none;"></div>
    </div>

    <script type="text/javascript">
        var Queries = getUrlVars();
        // tblCols 
        var tblCols = [];
        //var tblColsHeady = [];
        $("#inserty").click(function () {
            insertLoadFrm();
        });


        $(document).ready(function () {

            //give table title a name
            $(".entitySpan").text(Queries["Entity"]);

            //table in crud needs the first table head row so we need to load the header titles which is an alternate title to the column real names
            tblHeader();


        });

        //function insertEmp() {
        //    //Insert Form Load  
        //    $(".frm").html('');
        //    var elements = ["Email address", "First Name", "Last Name", "Adress", "City", "Lamborghini Mercy, Yourchick she so thirsty, I am in that two seat Lambo."];
        //    var arr = [];
        //    for (var i = 0; i < elements.length; i++) {
        //        arr.push(elements[i]);
        //    }
        //    var firstPart = '<div class="row"><div class="col-md-12"><div class="form-group"><label class="bmd-label-floating">';
        //    var secondPart = '</label><input type="text" class="form-control" id="txt' + i + '"/></div></div></div>';
        //    var lastPart = '<div class="row"><div class="col-md-12"><div class="form-group"><label>About You</label><div class="form-group"><label class="bmd-label-floating">Lamborghini Mercy, Yourchick she so thirsty, I am in that two seat Lambo.</label><textarea class="form-control" id="txtAbout" rows="5"></textarea></div></div></div></div>';
        //    var buttton = '<button type="submit" id="btnInsert" class="btn btn-primary pull-right">Insert</button><div class="clearfix"></div>';
        //    for (var i = 0; i < elements.length - 1 ; i++) {
        //        $('.frm').append(firstPart + arr[i] + secondPart);
        //    }
        //    $('.frm').append(lastPart + buttton);
        //    ///////////////////////////////////////////////////////////////////////////////
        //}

        // function to get the data in the same order as table header 
        function getRelatedHeader(forAy) {
            var relatedH = $.ajax({
                url: 'Service.asmx/getRelatedHeader',
                method: 'POST',
                data: { tblNameGen: Queries["Entity"], forA: forAy },
                dataType: 'xml',
                success: function (data) {
                    //tblCols = [];
                    var jqueryHeader = $(data);
                    for (var i = 0; i < jqueryHeader.find('string').length; i++) {
                        tblCols.push(jqueryHeader.find('string').eq(i).text());

                    }

                },
                error: function (err) {
                    //alert("error in adding Entity! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to get data in the right order, please try again');
                }
            });
            relatedH.done(

            );
        }


        //table data to be loaded after table header
        function getGen() {
            setTimeout(function () {
                $('#dataTable tbody tr').remove();
                //tblHeader();
                $.ajax({
                    beforeSend: function () {
                        //  get data in the same order
                        getRelatedHeader("body");
                    },
                    url: 'Service.asmx/selectGen',
                    method: 'post',
                    data: { tblNameGen: Queries["Entity"] },
                    dataType: 'xml',
                    success: function (data) {
                        //alert(tblCols);
                        var jqueryXML = $(data);
                        //count of rows data
                        var KeyD = jqueryXML.find("Table").length;
                        //count of table columns
                        var countx = $(data).find("Table").eq(0).children().length;

                        for (var i = 0; i < parseInt(KeyD) ; i++) {
                            $('#dataTable tbody').append($('<tr>'));

                            var rowData = jqueryXML.find('Table').eq(i).children();

                            for (var x = 0; x < countx - 1 ; x++) {
                                //alert(tblCols[x]);
                                //alert(rowData.find(tblCols[x]).text());

                                var TAGname = rowData.eq(x + 1).get(0).tagName;
                                var TAGToLower = TAGname.toLowerCase();

                                if (TAGToLower.includes("url") == true || TAGToLower.includes("image") == true) {
                                    //alert("beboo :" + countx);
                                    //var uploads = 'uploads/' + rowData.eq((x + 1)).text();
                                    var uploads = 'uploads/' + jqueryXML.find(tblCols[x]).eq(i).text();
                                    //alert(rowData.eq((x + 1)).text());
                                    $('#dataTable tbody tr:nth-of-type(' + (i + 1) + ')')
                                .append($('<td>').append($('<img>').attr("src", uploads).addClass('img-responsive').css({ "width": "80px" })));
                                }
                                else {
                                    //alert(TAGname);
                                    //alert(tblCols[x].eq(i).text());
                                    //alert(TAGname +"__"+ jqueryXML.find(tblCols[x]).eq(i).text());
                                    $('#dataTable tbody tr:nth-of-type(' + (i + 1) + ')')
                                //.append($('<td>').append(rowData.eq((x + 1)).text()));
                                .append($('<td>').append(jqueryXML.find(tblCols[x]).eq(i).text()));
                                }
                                //alert("x = " + x + " and i = " + i + " for tag is : " + TAGname);
                            }
                            //alert('edit ?');
                            $('#dataTable tbody tr:nth-of-type(' + (i + 1) + ')')
                                .append($('<td>').append($('<a>').attr('href', 'Update.aspx?ID=' + jqueryXML.find('ID').eq(i).text() + '&Entity=' + Queries["Entity"]).append($('<i>').attr('class', 'material-icons').text('edit')).attr("class", "myEditPen")))
                                .append($('<td>').append($('<div>').attr('ids', jqueryXML.find('ID').eq(i).text()).attr('class', 'btnDelete').attr('onclick', 'deleteGen(' + parseInt(jqueryXML.find("ID").eq(i).text()) + ');').append($('<i>').attr('class', 'fa fa-trash myDeletBin').attr('style', "font-size: 24px; color: red;     cursor: pointer;"))))
                                .append($('<input>').val(jqueryXML.find('ID').eq(i).text()).attr('type', 'hidden').addClass('idHidden'));
                        }


                        tblCols = [];
                    },
                    error: function (err) {
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed in retrieving Entity, please try again');
                    }
                });
                applyAccess();
            }, 50);
        }
        //function for access for edit, delete, insert or select
        function applyAccess() {

            //alert(editAccess);
            //alert(viewAccess);
            //alert(insertAccess);
            //alert(deleteAccess);

            $("body").bind("DOMNodeInserted", function () {
                //$(this).find('.myDeletBin').addClass('disabled');
                var editAccess = '<%= Session["editAccess"] %>';
                var viewAccess = '<%= Session["viewAccess"] %>';
                var insertAccess = '<%= Session["insertAccess"] %>';
                var deleteAccess = '<%= Session["deleteAccess"] %>';
                if (editAccess == 0) {
                    $(this).find('.myEditPen').addClass('disabled');
                }
                if (deleteAccess == 0) {
                    $(this).find('.btnDelete').addClass('disabled');
                    $(this).find('.btnDelete').unbind("click");
                }
                if (insertAccess == 0) {
                    $("#inserty").addClass("disabled");
                }
            });
        }
        //table in crud needs the first table head row so we need to load the header titles which is an alternate title to the column real names
        function tblHeader() {
            $('#tableHeader').html('');
            var Queries = getUrlVars();
            var ajaxHeaderReq =
                $.ajax({
                    beforeSend: function () {
                        //  get data in the same order
                        getRelatedHeader("header");
                        //if (tblColsHeady[0] == "" || tblColsHeady[0] == null) {
                        //    alert('empty');
                        //    location.reload();
                        //}
                    },
                    url: 'Service.asmx/getTblColums',
                    method: 'post',
                    data: { tableName: Queries["Entity"] },
                    dataType: 'xml',
                    success: function (data) {
                        var jqueryXMLHead = $(data);
                        //alert(KeyDHead);
                        var KeyDHead = jqueryXMLHead.find("Table").length;
                        //alert(KeyDHead);
                        for (var i = 0; i < parseInt(KeyDHead) ; i++) {
                            $('#tableHeader')
                            //.append($('<th>').append(jqueryXMLHead.find('COLUMN_NAME').eq(i + 1).text()))
                            .append($('<th>').append(tblCols[i]))
                            //tblCols.push(jqueryXMLHead.find('colName').eq(i).text());
                        }
                        $('#tableHeader').append($('<th>').attr('colspan', '2').addClass('text-center').text('Edit'))

                    },
                    error: function (err) {
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed in retrieving table header, please try again');
                    }
                });
            //after we load table header we load the data of the table
            ajaxHeaderReq.done(
                function () {
                    tblCols = [];
                    getGen();

                }
            );
        }


        //function to read the query strings of and get it's values
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        //function to delete data
        // fake
        function deleteGen(IDDelete) {
            //alert("deleted");

            var tblNm = $(".entitySpan").eq(0).text();
            $.ajax({
                url: 'Service.asmx/deleteGen',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: tblNm, idDel: parseInt(IDDelete) }),
                dataType: 'json',
                success: function (data) {
                    //return false;
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('deleted succefuly');
                },
                error: function (err) {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to delete data, please try again');
                }
            });
            var form = $("#form1");
            var post_url = form.attr('action');
            var post_data = getGen();
            $.ajax({
                type: 'POST',
                url: post_url,
                data: post_data,
                success: function () {
                    //return false;
                }
            });
            return false;
        }
        //for the insert part of any data to table .. we need to create fields first
        function insertLoadFrm() {
            $('.inputs').html('');

            $.ajax({
                url: 'Service.asmx/getTblColsNew',
                method: 'post',
                data: { tableName: Queries["Entity"] },
                dataType: 'xml',
                success: function (data) {

                    //var tblCols = [];
                    var colDTypes = [];
                    var colWidth = [];
                    var colDataType = [];
                    var jqueryXMLHead = $(data);
                    //var KeyDHead = jqueryXMLHead.find('Table').find("TABLE_SCHEMA:empty").length;
                    var KeyDPH = jqueryXMLHead.find('Table').length;

                    var alCols = [];
                    for (var i = 0; i < parseInt(KeyDPH) ; i++) {
                        var fieldys = jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text();
                        if (fieldys == "textarea") {
                            var firstPart = '<div class="fieldy col-md-' + jqueryXMLHead.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="form-group"><label class="bmd-label-floating">';

                            var secondPart = '</label><textarea   class="form-control" colType="' + jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text() + '" for="' + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + '"  rows="5"></textarea></div></div>';

                            $('.inputs').append(firstPart + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + secondPart);
                            alCols.push(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text());
                        }
                        else if (fieldys == "image") {
                            //alert("DAKHAL rakam"+i);
                            var firstPart = '<div class="fieldy col-md-' + jqueryXMLHead.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="form-group"><label class="bmd-label-floating">';

                            var secondPart = '</label><input type="file"  style="width: 100% ; display: -webkit-inline-box;" class="form-control fileUp" colType="' + jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text() + '" for="' +
                                 jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + '"  /> <br />' +
                                 '</div></div>';
                            //var secondPart = '</label> <br />' +
                            //    '<input type="file"  style="width: 100% ; display: -webkit-inline-box;" class="form-control fileUp" colType="' + jqueryXML.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '" for="' +
                            //     jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '"  /> <br />' +
                            //     '<img src="uploads/' + textStr + '"  class="img-responsive" style="height:80px"/>' +
                            //     '</div></div></div>';

                            $('.inputs').append(firstPart + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + secondPart);
                            alCols.push(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text());
                        }

                        else if (fieldys == "select") {
                            var selectID = jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text();
                            //alert(selectID);
                            var list = $('#sl' + selectID).html();
                            var firstPart = '<div class="fieldy col-md-' + jqueryXMLHead.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="form-group"><label class="bmd-label-floating">';

                            var secondPart = '</label>' +
                                '<select   class="form-control" colType="' + jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text() + '" for="' + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + '">' +
                                //loadRelation(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text(), jqueryXMLHead.find('Table').eq(i).find("tblName").eq(0).text())
                                list
                                + '</select>' +
                                '</div></div>';

                            $('.inputs').append(firstPart + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + secondPart);
                            alCols.push(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text());
                        }
                        else {
                            var firstPart = '<div class="fieldy col-md-' + jqueryXMLHead.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="form-group"><label class="bmd-label-floating">';

                            var secondPart = '</label><input type="text" class="form-control" colType="' + jqueryXMLHead.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text() + '" for="' + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + '"/></div></div>';

                            $('.inputs').append(firstPart + jqueryXMLHead.find('Table').eq(i).find("colAlias").eq(0).text() + secondPart);
                            alCols.push(jqueryXMLHead.find('Table').eq(i).find("colName").eq(0).text());
                        }
                    }
                    $("#hiddenElm").attr('value', alCols.toString());

                    var buttton = '<button type="submit" id="btnInsert" class="btn btn-primary pull-right" onclick="btnInsert">Insert</button><div class="clearfix"></div>';

                    //$('.frm').append(buttton);


                },
                error: function (err) {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving table header, please try again');
                }
            });
        }
        //function to upload files to server
        $("#btnInsert").click(function () {
            var allCols = $("#hiddenElm").val();
            var tblColsValues = [];
            var tblColType = [];

            var arrColName = allCols.split(','); // empty string separator

            for (var i = 0; i < arrColName.length; i++) {
                if ($("#txt" + arrColName[i]).attr('colType') != 'image') {
                    tblColType.push($("#txt" + arrColName[i]).attr('colType'));
                    tblColsValues.push($("#txt" + arrColName[i]).val());
                }
            }

            var str = "";
            for (var xz = 0; xz < arrColName.length; xz++) {
                if ($("#txt" + arrColName[xz]).attr('colType') == 'image') {
                    tblColType.push($("#txt" + arrColName[xz]).attr('colType'));
                }
            }
            for (var xyz = 0; xyz < count(tblColType) ; xyz++) {
                //alert('has images becuse count is : ' + count(tblColType));
                var data = new FormData();
                var files = $(".fileUp").get(xyz).files;
                data.append("UploadedImage", files[xyz]);
                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: "Service.asmx/UploadFile",
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (data) {
                        var jqueryy = $(data);
                        var newName = $(jqueryy).find('string').text();
                        tblColsValues.push(newName);
                    }
                });
                if (xyz == count(tblColType) - 1) {

                    ajaxRequest.done(function (xhr, textStatus) {

                        for (var i = 0; i < tblColsValues.length ; i++) {
                            if (i == tblColsValues.length - 1) {
                                if (tblColType[i] == 'number') {
                                    str += tblColsValues[i];
                                }
                                else {
                                    str += "N'" + tblColsValues[i] + "'";
                                }

                            }
                            else {
                                if (tblColType[i] == 'number') {
                                    str += tblColsValues[i] + ",";
                                }
                                else {
                                    str += "N'" + tblColsValues[i] + "', ";
                                }
                            }

                        }
                        Queries = getUrlVars();
                        var tblNames = Queries["Entity"];

                        if (count(tblColType) > 0) {

                            var ajaxRequest2 = insertNewData(tblNames, allCols, str);
                        }
                    });
                }
            }
            for (var i = 0; i < tblColsValues.length ; i++) {
                if (i == tblColsValues.length - 1) {
                    if (tblColType[i] == 'number') {
                        str += tblColsValues[i];
                    }
                    else {
                        str += "N'" + tblColsValues[i] + "'";
                    }

                }
                else {
                    if (tblColType[i] == 'number') {
                        str += tblColsValues[i] + ",";
                    }
                    else {
                        str += "N'" + tblColsValues[i] + "', ";
                    }
                }

            }
            Queries = getUrlVars();
            var tblNames = Queries["Entity"];
            if (count(tblColType) == 0) {

                var ajaxRequest2 = insertNewData(tblNames, allCols, str);
            }




            return false;
        });
        //function to count number of fields that represents images in this table
        function count(myArrarr) {
            array_elements = myArrarr;


            var current = null;
            var cnt = 0;
            for (var i = 0; i < array_elements.length; i++) {
                if (array_elements[i] == 'image') {
                    cnt++;
                }
            }
            return cnt;

        }
        //function to insert data into database
        function insertNewData(tblNames, allCols, str) {

            var REQUESTInsert = $.ajax({
                beforeSend: function () {
                    //alert('start upload');
                    //uploadPic();
                },
                url: 'Service.asmx/inserGen',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: tblNames, cols: allCols, txt: str }),
                dataType: 'json',
                success: function (data) {
                    var newUrl = $(location).attr('href');
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('added succefuly to the ' + tblNames + 'table');

                    location.reload();

                },
                error: function (err) {
                    //alert("error in adding Entity! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to add data to the ' + tblNames + ' table, please try again');
                }

            });


            REQUESTInsert.done(function () {

                if (tblNames == "AccessLevel") {
                    //alert(tblNames);
                    var NEWAccessName = $("#txtaccessName").val();
                    //alert(NEWAccessName);

                    $.ajax({
                        url: 'Service.asmx/InsertNewAccessLEvel',
                        method: 'POST',
                        contentType: 'application/json;charset-utf-8',
                        data: JSON.stringify({ accessNew: NEWAccessName }),
                        dataType: 'json',
                        success: function (data) {
                            alert(data.d);
                        },
                        error: function (err) {
                            $('.myAlert.failed').css('display', 'block');
                            $('#failMsg').html('failed to add new column to the table, please try again');
                        }
                    });

                }
                else {
                    //alert("No Access Name");
                }
            })
        }

    </script>

</asp:Content>
