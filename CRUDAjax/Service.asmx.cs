﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.ComponentModel;
using System.Data;
using System.Web.Script.Services;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data.SqlClient;


namespace CRUDAjax
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]

    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service : WebService
    {
        //facebookSyncy facebookSyncy = new facebookSyncy();

        ClassCode codeClass = new ClassCode();
        public DataTable GetNullFilledDataTableForXML(DataTable dtSource)
        {
            try
            {
                DataTable dtTarget = dtSource.Clone();
                foreach (DataColumn col in dtTarget.Columns)
                    col.DataType = typeof(string);

                int colCountInTarget = dtTarget.Columns.Count;
                foreach (DataRow sourceRow in dtSource.Rows)
                {
                    DataRow targetRow = dtTarget.NewRow();
                    targetRow.ItemArray = sourceRow.ItemArray;

                    for (int ctr = 0; ctr < colCountInTarget; ctr++)
                        if (targetRow[ctr] == DBNull.Value)
                            targetRow[ctr] = String.Empty;

                    dtTarget.Rows.Add(targetRow);
                }

                return dtTarget;
            }
            catch (Exception ex)
            {
                DataTable emptyTBL = new DataTable();
                return emptyTBL;
            }

        }

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        public bool pathExists(string filePath)
        {
            try
            {
                bool exists = File.Exists(filePath);
                return exists;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        [WebMethod]
        public string UploadFile()
        {
            try
            {
                string fileNameNew = "";
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];
                    string fileWith = httpPostedFile.FileName;

                    if (httpPostedFile != null)
                    {
                        // Validate the uploaded image(optional)

                        // Get the complete file path
                        var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/uploads"), httpPostedFile.FileName);
                        string fileJust = Path.GetFileNameWithoutExtension(httpPostedFile.FileName);
                        string ext = Path.GetExtension(fileSavePath);
                        for (int i = 0; i < 3; i++)
                        {
                            if (pathExists(fileSavePath))
                            {
                                fileJust = fileJust + "new";
                                fileWith = fileJust + ext;
                                fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/uploads"), fileWith);
                                i = 0;
                            }
                            else
                            {
                                i = 3;
                            }
                        }
                        fileNameNew = fileWith;

                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath);
                    }
                }
                return fileNameNew;

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [WebMethod]
        public DataSet colNames(string tblName)
        {
            try
            {
                DataTable dtx = codeClass.SQLREAD("select * from relations where fromTbl = N'" + tblName + "' AND fromCol in (select colName from Permissions where tblName = N'" + tblName + "')").Tables[0];
                string colNam;
                string tblNam;
                DataSet dscolName = new DataSet();
                List<string> tbls = new List<string>(dtx.Rows.Count);
                for (int i = 0; i < dtx.Rows.Count; i++)
                {
                    colNam = dtx.Rows[i]["refCol"].ToString();
                    //tbls.Add(dtx.Rows[i]["refTbl"].ToString());
                    tblNam = dtx.Rows[i]["refTbl"].ToString();
                    if (tbls.Where(o => string.Equals(tblNam, o, StringComparison.OrdinalIgnoreCase)).Any())
                    {
                        //DataTable dtcolName = new DataTable();
                        //DataColumn dccolName = new DataColumn("refCol", typeof(String));
                        //dtcolName.Columns.Add(dccolName);
                        //DataRow drcolName = dtcolName.NewRow();

                        //drcolName["refCol"] = colNam;
                        //dscolName.Tables[tblNam].Rows.Add(drcolName.ItemArray);
                        //ds.Tables.Add(dt);
                    }
                    else
                    {
                        tbls.Add(dtx.Rows[i]["refTbl"].ToString());
                        DataTable dtcolName = codeClass.SQLREAD("select * from " + tblNam).Tables[0].Copy();
                        dtcolName.TableName = tblNam;
                        dscolName.Tables.Add(dtcolName);
                    }

                }

                return dscolName;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }
        [WebMethod]
        public DataSet relatedTbls(string tblName)
        {
            try
            {
                DataSet ds = codeClass.SQLREAD("select * from relations where fromTbl = N'" + tblName + "' AND fromCol in (select colName from Permissions where tblName = N'" + tblName + "')");
                return ds;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }
        }

        [WebMethod]
        public int inserEmp(string colvalues, string colnames)
        {
            try
            {
                int id = codeClass.SQLGetLastID("INSERT INTO Employee (" + colnames + ") VALUES (" + colvalues + ") SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]");
                return id;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }
        //int Id,string Email, string Fname, string Lname, string Address, string City, string About

        [WebMethod]
        public void updateEmp(int idUp, string txt)
        {
            try
            {
                //email = '"+Email+"',fName = '"+Fname+ "',lName = '"+Lname+"',address = '"+Address+ "',city = '"+City+ "',about = '"+About+"'
                codeClass.SQLINSERT("Update Employee SET " + txt + " Where ID = " + idUp);
            }
            catch (Exception ex)
            {

            }

        }

        [WebMethod]
        public void deleteEmp(int idDel)
        {
            try
            {
                codeClass.SQLINSERT("Delete from Employee Where ID = " + idDel);
            }
            catch (Exception ex)
            {

            }

        }

        [ScriptMethod()]

        [WebMethod]
        public DataSet selectEmps()
        {
            try
            {
                DataSet ds = codeClass.SQLREAD("Select * from Employee");
                DataTable dt = GetNullFilledDataTableForXML(ds.Tables[0]);
                DataSet dsNew = new DataSet();
                dsNew.Tables.Add(dt);
                return dsNew;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }

        //public List<string> SearchCity(string prefixText, int count)
        //{
        //    List<string> customers = new List<string>();
        //    DataTable dtx = codeClass.SQLREAD("SELECT Distinct CITY_NAME_EN FROM Cities where city_name_En LIKE '%" + prefixText + "%'").Tables[0];
        //    foreach (DataRow row in dtx.Rows)
        //    {
        //        customers.Add(row["City_name_en"].ToString());
        //    }
        //    return customers;
        //}

        //[WebMethod]
        //public void postFace(string pgID, string msg, string AppID, string AppSec, string AccToken)
        //{
        //    facebookSyncy.post2Face(pgID, msg, AppID, AppSec, AccToken);

        //}

        [WebMethod]
        public DataSet getEmp(int ID)
        {
            try
            {
                return codeClass.SQLREAD("SELECT * from Employee Where ID = " + ID);

            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }
        }

        [WebMethod]
        public int tableColumnNums(string tableName)
        {
            try
            {
                int count = 0;
                string command = @"SELECT Col.Column_Name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab,INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col 
                             WHERE 
                             Col.Constraint_Name = Tab.Constraint_Name
                             AND 
                             Col.Table_Name = Tab.Table_Name
                             AND
                             Col.Table_Name = '" + tableName + "'";
                DataSet ds = codeClass.SQLREAD(command);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    count++;
                }

                return count;

            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        [WebMethod]
        public DataSet getTblCols(string tableName)
        {
            try
            {
                string command = "SELECT * FROM DB_A1715B_dynamics.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + tableName + "'";
                //string command = "SELECT colAlias FROM tblHelper WHERE tblName = N'" + tableName + "'";
                DataSet ds = codeClass.SQLREAD(command);
                DataSet ds2 = codeClass.SQLREAD("select * from tblHelper where tblName = N'" + tableName + "'");
                ds.Merge(ds2);
                DataTable dt = GetNullFilledDataTableForXML(ds.Tables[0]);
                DataSet dsNew = new DataSet();
                dsNew.Tables.Add(dt);
                return dsNew;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }
        [WebMethod]
        public DataSet getTblColsNew(string tableName)
        {
            try
            {
                string command = "select * from tblHelper where tblName = N'" + tableName + "' AND   enabledView = N'YES'";
                //string command = "SELECT colAlias FROM tblHelper WHERE tblName = N'" + tableName + "'";
                DataSet ds = codeClass.SQLREAD(command);
                DataTable dt = GetNullFilledDataTableForXML(ds.Tables[0]);
                DataSet dsNew = new DataSet();
                dsNew.Tables.Add(dt);
                return dsNew;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }

        [WebMethod]

        public DataSet getTblColums(string tableName)
        {
            try
            {
                //string command = "SELECT * FROM DB_A1715B_dynamics.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + tableName + "'";
                string command = "SELECT colAlias, colName FROM tblHelper WHERE tblName = N'" + tableName + "' AND priorityView = N'First' AND   enabledView = N'YES' AND colName in (select colName from Permissions where tblName = N'" + tableName + "') ORDER BY ID Asc";
                DataSet dsTblCol = codeClass.SQLREAD(command);
                DataTable dtTblCol = GetNullFilledDataTableForXML(dsTblCol.Tables[0]);
                DataSet dsNewTblCol = new DataSet();
                dsNewTblCol.Tables.Add(dtTblCol);
                return dsNewTblCol;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }

        [WebMethod]
        public DataSet getAllTables(string accLevTbls)
        {
            try
            {
                //string command = "SELECT  INFORMATION_SCHEMA.TABLES.TABLE_NAME, dbo.icons.icon_Name FROM INFORMATION_SCHEMA.TABLES INNER JOIN dbo.icons ON INFORMATION_SCHEMA.TABLES.TABLE_NAME = dbo.icons.tableName";
                string commandTbls = "select * from icons where accessLevel = N'" + accLevTbls + "'";
                DataSet dsTbls = codeClass.SQLREAD(commandTbls);
                DataTable dtTbls = GetNullFilledDataTableForXML(dsTbls.Tables[0]);
                DataSet dsNewTbls = new DataSet();
                dsNewTbls.Tables.Add(dtTbls);
                return dsNewTbls;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }

        //for all the generic CRUD functions
        [WebMethod]
        public string[] getRelatedHeader(string tblNameGen, string forA)
        {
            try
            {
                DataSet ds = codeClass.SQLREAD("select * from tblHelper where tblName = N'" + tblNameGen + "' AND  priorityView = N'First' AND   enabledView = N'YES' AND colName in (select colName from Permissions where tblName = N'" + tblNameGen + "')");
                var myList = new List<string>();


                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    if (item["FieldType"].ToString() == "select" && forA != "header")
                    {
                        string itemy = item["colName"].ToString();
                        DataSet dsy = codeClass.SQLREAD("select * from relations where fromTbl = N'" + tblNameGen + "' AND fromCol = N'" + itemy + "'");
                        myList.Add(dsy.Tables[0].Rows[0]["AliasCol"].ToString());

                    }
                    if (forA == "header")
                    {
                        myList.Add(item["colAlias"].ToString());
                    }
                    if (forA != "header" && item["FieldType"].ToString() != "select")
                    {
                        myList.Add(item["colName"].ToString());
                    }
                }

                //string[] tblHeaders = new string[] { "AccessID","tblName","colName","allowed" };
                string[] tblHeaders = myList.ToArray();

                return tblHeaders;
            }
            catch (Exception)
            {

                throw;
            }
        }

        [WebMethod]
        public DataSet selectGen(string tblNameGen)
        {
            try
            {
                DataSet dsRelGen = codeClass.SQLREAD("select * from tblHelper where tblName = N'" + tblNameGen + "' AND  refQuery IS NOT NULL  AND colName in (select colName from Permissions where tblName = N'" + tblNameGen + "')");
                DataSet dsNewGen = new DataSet();
                DataSet dsGen;
                DataTable dtGen;
                if (dsRelGen == null || dsRelGen.Tables[0] == null || dsRelGen.Tables[0].Rows.Count == 0)
                {

                    DataSet dsColsGen = codeClass.SQLREAD("select * from tblHelper where tblName =  N'" + tblNameGen + "'  AND   priorityView = N'First' AND   enabledView = N'YES'  AND colName in (select colName from Permissions where tblName = N'" + tblNameGen + "')");

                    // get all table columns
                    var colsTblGen = new List<string>();
                    for (int xGen = 0; xGen < dsColsGen.Tables[0].Rows.Count; xGen++)
                    {
                        colsTblGen.Add(dsColsGen.Tables[0].Rows[xGen]["colName"].ToString());
                    }
                    //string[] arraycolTbls = colsTbl.ToArray();
                    string colsStrGen = string.Join(",", colsTblGen.ToArray());
                    dsGen = codeClass.SQLREAD("Select ID," + colsStrGen + " from " + tblNameGen + " ORDER BY ID Asc ");
                    dtGen = GetNullFilledDataTableForXML(dsGen.Tables[0]);

                }
                else
                {
                    // get columns that have relations
                    var colRelatedGen = new List<string>();
                    for (int iGen = 0; iGen < dsRelGen.Tables[0].Rows.Count; iGen++)
                    {
                        colRelatedGen.Add(dsRelGen.Tables[0].Rows[iGen]["colName"].ToString());
                    }
                    string[] arraycolRelatedGen = colRelatedGen.ToArray();

                    DataSet dsColsGen = codeClass.SQLREAD("select * from tblHelper where tblName =  N'" + tblNameGen + "'  AND   priorityView = N'First' AND   enabledView = N'YES'  AND colName in (select colName from Permissions where tblName = N'" + tblNameGen + "')");

                    // get all table columns
                    var colsTblGen = new List<string>();
                    for (int xGen = 0; xGen < dsColsGen.Tables[0].Rows.Count; xGen++)
                    {
                        colsTblGen.Add(dsColsGen.Tables[0].Rows[xGen]["colName"].ToString());
                    }
                    string[] arraycolTblsGen = colsTblGen.ToArray();
                    //final array
                    string[] items2Gen = new string[arraycolTblsGen.Count()];
                    string[] oldValGen = new string[arraycolRelatedGen.Count()];
                    string[] newValuGen = new string[arraycolRelatedGen.Count()];
                    for (int zGen = 0; zGen < arraycolRelatedGen.Count(); zGen++)
                    {
                        foreach (var arrGen in arraycolTblsGen)
                        {
                            if (arrGen == arraycolRelatedGen[zGen])
                            {
                                string newValGen = codeClass.SQLREAD("Select * from tblHelper where tblName = N'" + tblNameGen + "' AND colName = N'" + arrGen + "'  AND colName in (select colName from Permissions where tblName = N'" + tblNameGen + "')").Tables[0].Rows[0]["refQuery"].ToString();
                                oldValGen[zGen] = arrGen;
                                newValuGen[zGen] = newValGen;
                            }
                        }
                    }
                    items2Gen = arraycolTblsGen.ToArray();
                    for (int yGen = 0; yGen < arraycolRelatedGen.Count(); yGen++)
                    {
                        foreach (var pGen in items2Gen.Where(c => c == oldValGen[yGen]))
                        {
                            int indeGen = Array.IndexOf(items2Gen, pGen);
                            items2Gen[indeGen] = newValuGen[yGen];
                        }
                        //items2 = arraycolTbls.Select(x => x.Replace(oldVal[y], newValu[y])).ToArray();


                    }
                    string result1Gen = ConvertStringArrayToString(items2Gen);
                    dsGen = codeClass.SQLREAD("select ID, " + result1Gen + " from " + tblNameGen);
                    dtGen = GetNullFilledDataTableForXML(dsGen.Tables[0]);

                }

                dsNewGen.Tables.Add(dtGen);
                return dsNewGen;

            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }
        static string ConvertStringArrayToString(string[] array)
        {
            try
            {
                // Concatenate all the elements into a StringBuilder.
                StringBuilder builder = new StringBuilder();
                foreach (string value in array)
                {
                    builder.Append(value);
                    builder.Append(',');
                }
                builder = builder.Remove(builder.Length - 1, 1);
                return builder.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        [WebMethod]
        public DataSet selectGenById(string tblName, int ID)
        {
            try
            {
                DataSet ds2 = codeClass.SQLREAD("select * from tblHelper where tblName = N'" + tblName + "' AND enabledView = 'YES' AND colName in (select colName from Permissions where tblName = N'" + tblName + "')");
                int county = ds2.Tables[0].Rows.Count;
                List<string> cols = new List<string>();
                foreach (DataRow item in ds2.Tables[0].Rows)
                {
                    cols.Add(item["colName"].ToString());
                }
                string colsStr = string.Join(",", cols.ToArray());
                DataSet ds = codeClass.SQLREAD("Select ID," + colsStr + " from " + tblName + " where ID = " + ID);
                ds.Merge(ds2);
                DataTable dt = GetNullFilledDataTableForXML(ds.Tables[0]);
                DataSet dsNew = new DataSet();
                dsNew.Tables.Add(dt);
                return dsNew;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }
        }

        [WebMethod]
        public void deleteGen(string tblName, int idDel)
        {
            try
            {
                if (tblName == "relations")
                {
                    DataSet ds = codeClass.SQLREAD("select * from relations where ID = " + idDel);
                    string tblFrom = ds.Tables[0].Rows[0]["fromTbl"].ToString();
                    string colFrom = ds.Tables[0].Rows[0]["fromCol"].ToString();
                    codeClass.SQLINSERT("Delete from " + tblName + " Where ID = " + idDel);

                    string str = "update tblHelper set FieldType = N'text',  refQuery = NULL where tblName = N'" + tblFrom + "' and colName = N'" + colFrom + "'";
                    codeClass.SQLINSERT(str);
                }
                else
                {
                    if (tblName == "AccessLevel")
                    {
                        codeClass.SQLINSERT("Delete from Permissions Where AccessID = " + idDel);
                    }

                    codeClass.SQLINSERT("Delete from " + tblName + " Where ID = " + idDel);
                }

            }
            catch (Exception ex)
            {

            }
        }

        [WebMethod]
        public int inserGen(string tblName, string cols, string txt)
        {
            try
            {
                int id = codeClass.SQLGetLastID("INSERT INTO " + tblName + " (" + cols + ") VALUES (" + txt + ") SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]");
                return id;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }

        [WebMethod]
        public void updateGen(string tblName, int idUp, string txt)
        {
            try
            {
                //email = '"+Email+"',fName = '"+Fname+ "',lName = '"+Lname+"',address = '"+Address+ "',city = '"+City+ "',about = '"+About+"'
                string queryUp = "Update " + tblName + " SET " + txt + " Where ID = " + idUp;
                codeClass.SQLINSERT(queryUp);
            }
            catch (Exception ex)
            {

            }
        }

        [WebMethod]
        public void updateTblHelper(string tblName, string colName, string DT)
        {
            try
            {
                //email = '"+Email+"',fName = '"+Fname+ "',lName = '"+Lname+"',address = '"+Address+ "',city = '"+City+ "',about = '"+About+"'
                string queryUp = "Update tblHelper SET DataType= N'" + DT + "' Where tblName = N'" + tblName + "' and colName=N'" + colName + "';";
                codeClass.SQLINSERT(queryUp);
            }
            catch (Exception ex)
            {

            }
        }

        [WebMethod]
        public void deleteTblHelper(string tblNam, string colNam)
        {
            try
            {
                codeClass.SQLINSERT("Delete from tblHelper Where tblName = N'" + tblNam + "' and colName = N'" + colNam + "'");
            }
            catch (Exception ex)
            {

            }

        }

        [WebMethod]
        public void deleteallTblfrmTblHeader(string tblName)
        {
            try
            {
                codeClass.SQLINSERT("Delete from tblHelper Where tblName = N'" + tblName + "';");
            }
            catch (Exception ex)
            {

            }
        }

        // tblHelper Funcyions

        [WebMethod]
        public void inserTblHelper(string cols, string txt)
        {
            try
            {
                codeClass.SQLINSERT("INSERT INTO tblHelper (" + cols + ") VALUES (" + txt + ") ");
            }
            catch (Exception ex)
            {

            }

        }

        [WebMethod]
        public DataSet getTblHelperData(string tableName)
        {
            try
            {
                DataSet ds = codeClass.SQLREAD("select * from tblHelper where tblName = '" + tableName + "'");
                DataTable dt = GetNullFilledDataTableForXML(ds.Tables[0]);
                DataSet dsNew = new DataSet();
                dsNew.Tables.Add(dt);
                return dsNew;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }
        // for general database management functions

        [WebMethod]
        public void colReorder(string valueStr)
        {
            try
            {
                string dsDrop;
                string sqlQuery = "UPDATE e SET colOrder = t.colOrder FROM tblHelper e JOIN ( VALUES " + valueStr + " ) t (tblName, colName, colOrder) ON t.colName = e.colName";
                dsDrop = codeClass.SQLINSERT(sqlQuery);
            }
            catch (Exception ex)
            {

            }


        }

        [WebMethod]
        public DataSet selectDBTables()
        {
            try
            {
                DataSet ds = codeClass.SQLREAD("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG = 'DB_A1715B_dynamics'");
                DataTable dt = GetNullFilledDataTableForXML(ds.Tables[0]);
                DataSet dsNew = new DataSet();
                dsNew.Tables.Add(dt);
                return dsNew;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }

        [WebMethod]
        public DataSet selectTblCols(string tblName)
        {
            try
            {
                DataSet ds = codeClass.SQLREAD("SELECT * FROM DB_A1715B_dynamics.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + tblName + "'");
                DataTable dt = GetNullFilledDataTableForXML(ds.Tables[0]);
                DataSet dsNew = new DataSet();
                dsNew.Tables.Add(dt);
                return dsNew;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }

        [WebMethod]
        public DataSet selectTblData(string tblName)
        {
            try
            {
                DataSet ds = codeClass.SQLREAD("SELECT * FROM  N'" + tblName + "'");
                DataTable dt = GetNullFilledDataTableForXML(ds.Tables[0]);
                DataSet dsNew = new DataSet();
                dsNew.Tables.Add(dt);
                return dsNew;
            }
            catch (Exception ex)
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }

        }

        [WebMethod]
        public void createTbl(string tblName, string colData)
        {
            try
            {
                string sqlQuer = " CREATE TABLE " + tblName + " ( " + colData + " ) ";
                DataSet ds = codeClass.SQLREAD(sqlQuer);

            }
            catch (Exception ex)
            {

            }

        }

        [WebMethod]
        public string deleteTbl(string tblName)
        {
            try
            {
                string dsDrop = "";
                DataSet dsCheckDel = codeClass.SQLREAD("select * from relations where refTbl = N'" + tblName + "'");
                if (dsCheckDel == null || dsCheckDel.Tables[0] == null || dsCheckDel.Tables[0].Rows.Count == 0)
                {
                    string qur = "DROP TABLE " + tblName;
                    dsDrop = codeClass.SQLINSERT(qur);

                    string qurRel = "delete from relations where fromTbl='" + tblName + "'";
                    dsDrop = codeClass.SQLINSERT(qurRel);

                    string qurPerm = "delete from Permissions where tblName='" + tblName + "'";
                    dsDrop = codeClass.SQLINSERT(qurPerm);
                }
                else
                {
                    dsDrop = "Sorry " + tblName + " is related  to another table, remove the relation and try again!";
                }
                return dsDrop;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        [WebMethod]
        public string deleteRel(string tblName, string fType, string colName)
        {
            try
            {
                string dsDrop = " empty";
                DataSet dsCheckDel = codeClass.SQLREAD("select * from relations where fromTbl = N'" + tblName + "' AND fromCol = N'" + colName + "'");
                if (dsCheckDel == null || dsCheckDel.Tables[0] == null || dsCheckDel.Tables[0].Rows.Count == 0)
                {
                    //string qur = "DROP TABLE " + tblName;
                    //dsDrop = codeClass.SQLINSERT(qur);
                    //string qurRel = "delete from relations where fromTbl='" + tblName + "'";
                    //dsDrop = codeClass.SQLINSERT(qurRel);
                }
                else
                {
                    string qurRel = "delete from relations where fromTbl = N'" + tblName + "' AND fromCol = N'" + colName + "'";
                    dsDrop = codeClass.SQLINSERT(qurRel);
                    qurRel = "update tblHelper set FieldType = N'" + fType + "', refQuery = NULL where tblName = N'" + tblName + "' AND colName = N'" + colName + "'";
                    dsDrop = codeClass.SQLINSERT(qurRel);
                }
                return dsDrop;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public void addCol2Tbl(string tblName, string colName, string colData)
        {
            try
            {
                string dsAdd;
                string sqlQuery = "ALTER TABLE " + tblName + " ADD " + colName + " " + colData + " ";
                dsAdd = codeClass.SQLINSERT(sqlQuery);
            }
            catch (Exception ex)
            {

            }

        }

        [WebMethod]
        public string deleteCol(string tblName, string colName)
        {
            try
            {
                string dsDrop = "";
                DataSet dsCheckDel = codeClass.SQLREAD("select * from relations where refTbl = N'" + tblName + "' and refCol = N'" + colName + "'");
                if (dsCheckDel == null || dsCheckDel.Tables[0] == null || dsCheckDel.Tables[0].Rows.Count == 0)
                {
                    string sqlQuery = "ALTER TABLE " + tblName + " DROP COLUMN " + colName;
                    dsDrop = codeClass.SQLINSERT(sqlQuery);
                    string qurRel = "delete from relations where fromTbl='" + tblName + "' and fromCol='" + colName + "'";
                    dsDrop = codeClass.SQLINSERT(qurRel);
                }
                else
                {
                    dsDrop = "Sorry " + colName + " is related  to another table, remove the relation and try again!";
                }
                return dsDrop;

            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public void updateCol(string tblName, string colName, string colDataType, string colFType, string newName)
        {
            try
            {
                string dsDrop;
                string dsDrop1;
                string sqlQuery = "ALTER TABLE " + tblName + " ALTER COLUMN " + colName + " " + colDataType;
                dsDrop = codeClass.SQLINSERT(sqlQuery);
                //sqlQuery = "ALTER TABLE " + tblName + " ALTER COLUMN " + colName + " " + colDataType;
                //dsDrop = codeClass.SQLINSERT(sqlQuery);
                //update tblHelper set FieldType = N'' where tblName = N'Tably' and colName = N'tableName'
                string FDtype = "update tblHelper set FieldType = N'" + colFType + "' , DataType= N'" + colDataType + "' where tblName = N'" + tblName + "' and colName = N'" + colName + "'";
                //string queryUp = "Update tblHelper SET DataType= N'" + DT + "' Where tblName = N'" + tblName + "' and colName=N'" + colName + "';";

                dsDrop1 = codeClass.SQLINSERT(FDtype);
                if (newName != colName)
                {
                    codeClass.SQLINSERT("EXEC sp_rename 'dbo." + tblName + "." + colName + "', '" + newName + "', 'COLUMN';");
                    FDtype = "update tblHelper set colName = N'" + newName + "'  where tblName = N'" + tblName + "' and colName = N'" + colName + "'";
                    //string queryUp = "Update tblHelper SET DataType= N'" + DT + "' Where tblName = N'" + tblName + "' and colName=N'" + colName + "';";

                    dsDrop1 = codeClass.SQLINSERT(FDtype);
                }

            }
            catch (Exception ex)
            {

            }

        }

        protected DataSet checkViewExist(string viewName)
        {
            try
            {
                DataSet ds = codeClass.SQLREAD("SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'" + viewName + "')");
                return ds;
            }
            catch
            {
                DataSet emptyDS = new DataSet();
                return emptyDS;
            }
        }
        protected void ViewNew(string viewName, string selectStr)
        {
            try
            {
                string str = codeClass.SQLINSERT("CREATE VIEW " + viewName + " AS " + selectStr);
                inserTblView(viewName);
            }
            catch (Exception ex)
            {

            }
        }
        protected void ViewAlter(string viewName, string selectStr)
        {
            try
            {
                string str = codeClass.SQLINSERT("ALTER VIEW " + viewName + " AS " + selectStr);
            }
            catch (Exception ex)
            {

            }

        }
        // view web methods
        [WebMethod]
        public void createView(string viewName, string selectStr)
        {
            try
            {
                // selectStr = "SELECT
                // dbo.Employee.*, dbo.tblHelper.*, dbo.Department.ID as DepartID
                // FROM dbo.Department 
                // INNER JOIN dbo.Employee ON dbo.Department.ID = dbo.Employee.depID 
                // INNER JOIN dbo.tblHelper ON dbo.Employee.ID = dbo.tblHelper.ID"

                // selectStr = " SELECT  Employee.*,   Department.ID as DepartID  FROM  Department  INNER JOIN  Employee ON  Department.ID =  Employee.depID "
                //SELECT Employee.lName,Employee.fName,Department.depName FROM Employee INNER JOIN Department on Employee.depID = Department.ID
                //" CREATE OR REPLACE VIEW newViews AS   SELECT Employee.fName,Employee.lName,Employee.address,Department.depName  FROM Employee  INNER JOIN Department on Employee.depID = Department.ID  "
                //" IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'newview456'))  EXEC sp_executesql N' CREATE VIEW newview456 AS  SELECT Employee.fName,Employee.address,Employee.lName,Department.depName  FROM Employee  INNER JOIN Department on Employee.depID = Department.ID' GO ALTER VIEW newview456 AS  SELECT Employee.fName,Employee.address,Employee.lName,Department.depName  FROM Employee  INNER JOIN Department on Employee.depID = Department.ID GO "
                //string sqlQuer = " CREATE OR REPLACE VIEW " + viewName + " AS  " + selectStr + "  ";
                DataSet ds = checkViewExist(viewName);
                if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
                {
                    ViewNew(viewName, selectStr);
                }
                else
                {
                    ViewAlter(viewName, selectStr);
                }
                //string myQuery = " IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'" + viewName + "'))  EXEC sp_executesql N' CREATE VIEW " + viewName + " AS " + selectStr + "'" +
                // " GO" +
                // " ALTER VIEW " + viewName + " AS " + selectStr +
                // " GO ";
                ////string queryNew = @"DECLARE @text NVARCHAR(1000) " +
                ////                    " SET @text = " + myQuery +
                ////                    " SELECT @text";
                //myQuery = SplitGO(myQuery).ToString();
                //myQuery = SplitAlter(myQuery).ToString();

                //string ds = codeClass.SQLINSERT(myQuery);

            }
            catch (Exception ex)
            {

            }

        }
        private static string SplitGO(string sqlScript)
        {
            try
            {
                // Split by "GO" statements
                var statements = Regex.Split(
                        sqlScript,
                        @"^[\t ]*GO[\t ]*\d*[\t ]*(?:--.*)?$",
                        RegexOptions.Multiline |
                        RegexOptions.IgnorePatternWhitespace |
                        RegexOptions.IgnoreCase);

                // Remove empties, trim, and return
                statements.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.Trim(' ', '\r', '\n'));
                string singleString = string.Join(",", statements.ToArray());
                return singleString;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        private static string SplitAlter(string sqlScript)
        {
            try
            {
                // Split by "GO" statements
                var statements = Regex.Split(
                        sqlScript,
                        @"^[\t ]*Alter[\t ]*\d*[\t ]*(?:--.*)?$",
                        RegexOptions.Multiline |
                        RegexOptions.IgnorePatternWhitespace |
                        RegexOptions.IgnoreCase);

                // Remove empties, trim, and return

                statements.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.Trim(' ', '\r', '\n'));
                string singleString = string.Join(",", statements.ToArray());
                return singleString;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public void deleteView(string viewName)
        {
            try
            {
                string dsDrop;
                string sqlQuery = " DROP VIEW " + viewName;
                dsDrop = codeClass.SQLINSERT(sqlQuery);
            }
            catch (Exception ex)
            {

            }

        }

        [WebMethod]
        public void inserTblView(string viewName)
        {
            try
            {
                codeClass.SQLINSERT("INSERT INTO tblViews (viewName) VALUES (N'" + viewName + "') ");

            }
            catch (Exception ex)
            {

            }
        }
        // autocomplete text box

        [WebMethod]
        public List<string> autoTbls(string prefixText, int count)
        {
            try
            {
                List<string> tableNames = new List<string>();
                DataTable dtx = codeClass.SQLREAD("SELECT  INFORMATION_SCHEMA.TABLES.TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  where INFORMATION_SCHEMA.TABLES.TABLE_NAME LIKE '%" + prefixText + "%'").Tables[0];
                foreach (DataRow row in dtx.Rows)
                {
                    tableNames.Add(row["TABLE_NAME"].ToString());
                }
                return tableNames;
            }
            catch (Exception ex)
            {
                List<string> emptyLIST = new List<string>();
                return emptyLIST;
            }

        }

        // for testing purposes
        [WebMethod]
        public void UpdateExactRelation(string Tablename, string UpdateStatement, string colWhere, string searchkey, string checky, string txt, string refCol, string refTbl)
        {
            try
            {
                string refQuery = "(select " + refCol + " from " + refTbl + " where " + refTbl + ".ID = " + Tablename + "." + checky + ") AS " + refCol;
                DataSet ds = codeClass.SQLREAD("select * from relations where fromTbl = N'" + Tablename + "' AND fromCol = N'" + checky + "'");
                if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
                {
                    inserGen("relations", "fromTbl,fromCol,refTbl,refCol", txt);
                    string queryUp = "Update tblHelper SET refQuery = N'" + refQuery + "' Where  tblName = N'" + Tablename + "' AND colName = N'" + checky + "'";
                    codeClass.SQLINSERT(queryUp);
                }
                else
                {
                    codeClass.UpdateExact("relations", UpdateStatement, colWhere, searchkey);
                    //(select depName from Department where Department.ID = Employee.depID) AS DepName
                    string queryUp = "Update tblHelper SET refQuery = N'" + refQuery + "' Where  tblName = N'" + Tablename + "' AND colName = N'" + checky + "'";
                    codeClass.SQLINSERT(queryUp);

                }
            }
            catch (Exception ex)
            {

            }

        }


        [WebMethod]
        public int InsertNewAccessLEvel(string accessNew)
        {
            try
            {
                int maxID = 0;
                DataSet maxID_ds = codeClass.SQLREAD("SELECT ID from AccessLevel where accessName=N'" + accessNew + "';");
                foreach (DataRow row in maxID_ds.Tables[0].Rows)
                {
                    maxID = int.Parse(row["ID"].ToString());
                }
                //string command = "SELECT ID from AccessLevel where accessName=N'"+ accessNew + "';";
                //SqlCommand cmd = new SqlCommand(command);
                //maxID = (int)cmd.ExecuteScalar();
                //string tbls = "";
                DataSet ds = codeClass.SQLREAD("SELECT DISTINCT tblName FROM  tblHelper;");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["tblName"].ToString() != "AccessLevel")
                    {
                        //tbls += ds.Tables[0].Rows[i]["tblName"] +"-";
                        var query = "insert into Permissions (AccessID,tblName,colName,allowed) values (" + maxID + ",N'" + ds.Tables[0].Rows[i]["tblName"] + "',N'ID',N'YES')";

                        codeClass.SQLINSERT(query);
                    }

                }

                return maxID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        //functions for profile purposes
        [WebMethod(EnableSession = true)]
        public string loginReq(string user, string pass)
        {
            DataSet ds = codeClass.SQLREAD("select * from Users where email = N'" + user + "' and pass = N'" + pass + "'");
            string status = "not registered";
            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
            {

            }
            else
            {
                int userID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                Session["userID"] = userID;
                Session["AccessID"] = ds.Tables[0].Rows[0]["accountType"].ToString();
                DataSet dsID = codeClass.SQLREAD("select * from AccessLevel where ID = " + int.Parse(ds.Tables[0].Rows[0]["accountType"].ToString()));
                status = dsID.Tables[0].Rows[0]["accessName"].ToString();
                int accessID = int.Parse(Session["AccessID"].ToString());
                Session["accountType"] = status;
                Session["mail"] = user;
                Session["password"] = pass;
                checkAccess();
                Session.Timeout = 10080;
            }
            return status;
        }

        [WebMethod]
        public string registerReq(string fName, string lName, string email, string pass, string phone, int accountType, string ppUrl)
        {
            DataSet dscheckExist = codeClass.SQLREAD("select * from Users where email = N'" + email + "'");
            if (dscheckExist == null || dscheckExist.Tables[0] == null || dscheckExist.Tables[0].Rows.Count == 0)
            {
                int commandID = codeClass.SQLGetLastID("Insert INTO Users (fName,lName, email, pass, phone, accountType, ppUrl) values (N'" + fName + "' , N'" + lName + "' ,  N'" + email + "' , N'" + pass + "' , N'" + phone + "' , N'" + accountType + "',  N'" + ppUrl + "')  SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]");
                DataSet ds = codeClass.SQLREAD("select * from Users where ID = " + commandID);
                if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
                {
                    
                }
                else
                {
                    string user = ds.Tables[0].Rows[0]["email"].ToString();
                    string passy = ds.Tables[0].Rows[0]["pass"].ToString();
                    //loginReq(user, passy);
                }

                return "email Not Exist";
            }
            else
            {
                return "email Exist";
            }
            
        }

        // check access level authorization before page load
        [WebMethod(EnableSession = true)]
        public string checkLevel(string tblName)
        {
            string strString = "";
            int accessID = int.Parse(Session["AccessID"].ToString());
            DataSet ds = codeClass.SQLREAD("select * from Permissions where AccessID = " + accessID + " and tblName = N'" + tblName + "'");
            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
            {
                strString = "not permitted";
            }
            else
            {
                strString = "Allowed";
            }

            return strString;
        }
        [WebMethod(EnableSession = true)]
        public void checkAccess()
        {
            //Session["accountType"]
            DataSet ds = codeClass.SQLREAD("select * from accesslevel where accessName = N'" + Session["accountType"] + "'");
            Session["editAccess"] = int.Parse(ds.Tables[0].Rows[0]["accessEdit"].ToString());
            Session["viewAccess"] = int.Parse(ds.Tables[0].Rows[0]["accessView"].ToString());
            Session["insertAccess"] = int.Parse(ds.Tables[0].Rows[0]["accessInsert"].ToString());
            Session["deleteAccess"] = int.Parse(ds.Tables[0].Rows[0]["accessDelete"].ToString());


        }


    }
}
