﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="testingFile.aspx.cs" Inherits="CRUDAjax.testingFile" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row" style="margin-top: 150px;">
        <div class="col-sm-12">
            <input type="file" class="form-control fileUp" style="width: 25% ; display: -webkit-inline-box;" />
            <img src="uploads/1.jpg" class="img-responsive" style="height:80px"/>
        </div>
    </div>
    <div class="row">
        <button class="btn btn-success btnSubmit">submit</button>
    </div>
    <script>
        $('.btnSubmit').click(function () {
            var data = new FormData();
            var files = $(".fileUp").get(0).files;
            data.append("UploadedImage", files[0]);
            var ajaxRequest = $.ajax({
                type: "POST",
                url: "Service.asmx/UploadFile",
                contentType: false,
                processData: false,
                data: data
            });
            ajaxRequest.done(function (xhr, textStatus) {
                // Do other operation
            });
        });
    </script>
</asp:Content>
