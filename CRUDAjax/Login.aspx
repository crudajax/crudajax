﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CRUDAjax.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="icon" type="image/png" href="assets/img/favicon-96x96-new.png" />
    <title>Login | Dynawix Admin Panel</title>
    <link href="assets/css/material-dashboard.css" rel="stylesheet" />
    <link href="assets/css/login.css" rel="stylesheet" />
    <script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
    <script src="assets/js/core/bootstrap-material-design.min.js"></script>
    <script src="assets/js/material-dashboard.js"></script>
    <style>
        .vertical-menu {
            /*width: 200px;*/
            height: 322px;
            overflow-y: auto;
        }

        body select.input {
            display: inline-block;
            background-color: rgb(153, 50, 177);
            border-width: 0 0 1.7px 0;
            text-align: center;
            font-size: 22px !important;
            margin-top: 0 !important;
            text-transform: capitalize;
        }

        input:-internal-autofill-selected {
            background-color: rgba(0,0,0,0) !important;
        }

        body .myAlert {
            width: 80%;
            z-index: 8000;
            margin: auto;
            display: none;
        }
    </style>
</head>

<body>
    <form id="form1" runat="server">
        <%----------------------------------------------------- Alerts ------------------------------------------------%>
        <div class="alert alert-success alert-dismissible fade show myAlert success" role="alert">
            <strong>Good News!</strong> <span id="succMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger alert-dismissible fade show myAlert failed" role="alert">
            <strong>Sorry!</strong> <span id="failMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <%----------------------------------------------------- ------ ------------------------------------------------%>
        <div class="materialContainer">

            <div class="box">

                <div class="title">LOGIN</div>

                <div class="input">
                    <label for="name">E-mail</label>
                    <input type="text" name="name" id="name" />
                    <span class="spin"></span>
                </div>

                <div class="input">
                    <label for="pass">Password</label>
                    <input type="password" name="pass" id="pass" />
                    <span class="spin"></span>
                </div>

                <div class="button login">
                    <button class="loginUser"><span>LOGIN</span> <i class="fa fa-check"></i></button>
                </div>

                <p class="pass-forgot">Forgot your password?</p>

            </div>
            <div class="overbox">
                <div class="vertical-menu">
                    <div class="material-button alt-2"><span class="shape"></span></div>

                    <div class="title">REGISTER</div>

                    <div class="input">
                        <label for="regfname">first name</label>
                        <input type="text" name="regfname" id="regfname" />
                        <span class="spin"></span>
                    </div>

                    <div class="input">
                        <label for="reglname">last name</label>
                        <input type="text" name="reglname" id="reglname" />
                        <span class="spin"></span>
                    </div>

                    <div class="input">
                        <label for="regmail">e-mail</label>
                        <input type="email" name="regmail" id="regmail" />
                        <span class="spin"></span>
                    </div>

                    <div class="input">
                        <label for="regpass">Password</label>
                        <input type="password" name="regpass" id="regpass" />
                        <span class="spin"></span>
                    </div>

                    <div class="input">
                        <label for="reregpass">Repeat Password</label>
                        <input type="password" name="reregpass" id="reregpass" />
                        <span class="spin"></span>
                    </div>

                    <div class="input">
                        <label for="regphone">Phone</label>
                        <input type="text" name="regphone" id="regphone" />
                        <span class="spin"></span>
                    </div>

                    <div class="input">
                        <label for="regaccountType">Account Type</label>
                        <select name="regaccountType" id="regaccountType" class="input"></select>
                        <span class="spin"></span>
                    </div>

                    <div class="input">
                        <%--<label for="ProfilePicture">Profile Picture</label>--%>
                        <input type="file" name="ProfilePicture" id="ProfilePicture" />
                        <%--<span class="spin"></span>--%>
                    </div>

                </div>
                <div class="button">
                    <button class="regNew"><span>REGISTER</span></button>
                </div>
            </div>
        </div>
    </form>
    <script src="assets/js/login.js"></script>

    <script>
        


        $(document).ready(function () {
            // clearSess();
            $.ajax({
                url: 'Service.asmx/selectGen',
                method: 'post',
                data: { tblNameGen: "AccessLevel" },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    //count of rows data
                    var KeyD = jqueryXML.find("Table").length;
                    //count of table columns
                    var countx = $(data).find("Table").eq(0).children().length;

                    var options = "";
                    for (var i = 0; i < KeyD; i++) {
                        options += "<option value='" + jqueryXML.find("Table").eq(i).find("ID").text() + "'>" + jqueryXML.find("Table").eq(i).find("accessName").text() + "</option>";
                    }
                    //alert(options);
                    $('#regaccountType').append(options);
                    $('#regaccountType').prepend($("<option>").text('Select Your Level').attr('value', '-1').css("font-size", "22px !important;").attr('selected', 'selected'));
                },
                error: function (err) {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to add new column to the table, please try again');
                }
            });

            $(".regNew").click(function () {
                //alert("clicked");
                var fname = $('#regfname').val();
                var lname = $('#reglname').val();
                var mail = $('#regmail').val();
                var password = $('#regpass').val();
                var phone = $('#regphone').val();
                var account = $('#regaccountType option:selected').val();
                var pictURL = "no image";
                //alert(fname+ "_" + lname + "_" + mail + "_" + password + "_" + phone + "_" + account );

                $.ajax({
                    url: 'Service.asmx/registerReq',
                    method: 'post',
                    data: { fName: fname, lName: lname, email: mail, pass: password, phone: phone, accountType: account, ppUrl: pictURL },
                    dataType: 'xml',
                    success: function (data) {
                        
                        if (data.d == "email Not Exist") {
                            //alert("Success");
                            $('.myAlert.success').css('display', 'block');
                            $('#succMsg').html('succefuly Register .... waiting confirm from super admin');
                        }
                        else if (data.d = "email Exist") {
                            $('.myAlert.failed').css('display', 'block');
                            $('#failMsg').html('this email is used by another user,change your mail account');
                        }
                    },
                    error: function (err) {
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed to register, please try again');
                    }
                });
                //return false;
            });

            $(".loginUser").click(function () {
                var mail = $('#name').val();
                var password = $('#pass').val();

                $.ajax({
                    url: 'Service.asmx/loginReq',
                    method: 'POST',
                    contentType: 'application/json;charset-utf-8',
                    data: JSON.stringify({ user: mail, pass: password }),
                    dataType: 'json',
                    success: function (data) {
                        if (data.d == "not registered") {
                            $('.myAlert.failed').css('display', 'block');
                            $('#failMsg').html('credentials does not match, please try again');
                        }
                        else {
                            //$('.myAlert.success').css('display', 'block');
                            //$('#succMsg').html('succefuly login');
                            //var ID = '<%= Session["userID"] %>';
                            $(location).attr("href", "Default.aspx");
                        }
                        //alert(data.d);

                    },
                    error: function (err) {
                        //alert(err.responseText);
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed to login, please try again');
                    }
                });

                return false;
            });

        });
    </script>
</body>
</html>
