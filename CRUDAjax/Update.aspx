﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="Update.aspx.cs" Inherits="CRUDAjax.Update" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
    Update data
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .cont {
            padding-top: 90px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }

        body .fieldy {
            display: -webkit-inline-box;
        }

        body .myAlert {
            width: 80%;
            z-index: 8000;
            margin: auto;
            display: none;
        }
        
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cont" id="contLoaded">
        <div class="alert alert-success alert-dismissible fade show myAlert success" role="alert">
            <strong>Good News!</strong> <span id="succMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger alert-dismissible fade show myAlert failed" role="alert">
            <strong>Sorry!</strong> <span id="failMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>

        <%-- ahmed --%>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Update <span class="entitySpan"></span></h4>
                                <p class="card-category">Update your profile</p>
                            </div>
                            <div class="card-body">
                                <panel id="pnlGeneric">
                                    <input type="hidden" id="txtID" />
                                    <%--<div class="row"><div class="col-md-12"><div class="form-group"><input type="email" id="txtMail" class="form-control" placeholder="Email address" /></div></div></div>--%>
                                    <button type="submit" id="btnUpdate" class="btn btn-primary pull-right">Update</button>
                                    <div class="clearfix"></div>
                                </panel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="relationHdn" style="display: none;"></div>

    </div>

    <script>

        $(document).ready(function () {
            var Queries = getUrlVars();
            var tblName = Queries["Entity"];
            if (tblName == "relations" || tblName == "tblHelper") {
                redirecty();
                //$('.myAlert.failed').css('display', 'block');
                //$('#failMsg').html('This page is inaccessible, you would be redirected to homepage!');
                //$('.myAlert.failed').append($('<button>').text('OK').addClass('btn btn-primary text-center').attr('onclick', 'redirecty();'));
            }
            else {
                loadRelation(tblName);
            }
        });
        function redirecty() {
            location.href = "Default.aspx";
        }
        //function used to store query strings
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        // function used to know which columns in the desired table is related to another table
        function loadRelation(tblName) {
            var trial = "";
            var relatedTbls = [];
            var relatedColsID = [];
            var relatedColsName = [];
            //get column names that are related
            $.ajax({
                url: 'Service.asmx/relatedTbls',
                method: 'post',
                data: { tblName: tblName },
                dataType: 'xml',
                success: function (data) {
                    var jqueryTbls = $(data);
                    for (var i = 0; i < jqueryTbls.find('Table').length; i++) {
                        relatedTbls.push(jqueryTbls.find('Table').eq(i).find('refTbl').eq(0).text());
                        relatedColsID.push(jqueryTbls.find('Table').eq(i).find('refCol').eq(0).text());
                        relatedColsName.push(jqueryTbls.find('Table').eq(i).find('fromCol').eq(0).text());
                    }
                },
                error: function (err) {
                    //alert("error in retrieving relation! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').append('there isn\'t any relation in this table');
                }
            });
            //get related data in the related table to be choosen on update
            $.ajax({
                url: 'Service.asmx/colNames',
                method: 'post',
                data: { tblName: tblName },
                dataType: 'xml',
                success: function (data) {
                    //for (var x = 0; x < relatedTbls.length; x++) {
                    //    var trial = "";
                    //    var jqueryHead = $(data);
                    //    for (var i = 0; i < jqueryHead.find(relatedTbls[x]).length; i++) {
                    //        trial += '<option value="' + jqueryHead.find(relatedTbls[x]).eq(i).find("ID").eq(0).text() + '">' + jqueryHead.find(relatedTbls[x]).eq(i).find(relatedColsID[x]).eq(0).text() + '</option>';
                    //    }
                    //    $('#relationHdn').append($('<select>').attr('id', 'sl' + relatedColsName[x]).html(trial));
                    //}
                    for (var x = 0; x < relatedTbls.length; x++) {
                        var trial = "";
                        var jqueryHead = $(data);
                        for (var i = 0; i < jqueryHead.find(relatedTbls[x]).length; i++) {
                            var valRela = jqueryHead.find(relatedTbls[x]).eq(i).find("ID").eq(0).text();
                            if (relatedColsName[x].match("^access")) {
                                valRela = parseInt(jqueryHead.find(relatedTbls[x]).eq(i).find("bitValue").eq(0).text());
                            }
                            trial += '<option value="' + valRela + '">' + jqueryHead.find(relatedTbls[x]).eq(i).find(relatedColsID[x]).eq(0).text() + '</option>';
                        }
                        $('#relationHdn').append($('<select>').attr('id', 'sl' + relatedColsName[x]).html(trial));
                    }
                },
                error: function (err) {
                    //alert("error in retrieving relation! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').append('there aren\'t any related fields here ');
                }

            });

        }
    </script>
    <script type="text/javascript">
        $(window).load(function () {
            $('.inputs').html('');
            var entityTbl = GetParameterValues('Entity');
            if (entityTbl == "tblHelper") {
                //countx = $(data).find("Table").eq(0).children().length;
                ////jqueryXML.find("Table").eq(x).find('colAlias').eq(0).text()

            }


            //function used to store query strings
            function GetParameterValues(param) {
                var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < url.length; i++) {
                    var urlparam = url[i].split('=');
                    if (urlparam[0] == param) {
                        return urlparam[1];
                    }
                }
            }

            var id = GetParameterValues('ID');

            $(".entitySpan").text(entityTbl);
            // load data of specified item that needs edit
            var ajaxDone = $.ajax({
                url: 'Service.asmx/selectGenById',
                method: 'post',
                data: { tblName: entityTbl, ID: id },
                dataType: 'xml',
                success: function (data) {
                    var colDataType = [];
                    var countx;
                    var alCols = [];
                    var arrRelations = [];
                    if (entityTbl == "tblHelper") {
                        countx = $(data).find("Table").eq(0).children().length;
                    }
                    else {
                        countx = parseInt($(data).find("Table").eq(0).children().length) - 6;
                    }

                    var jqueryXML = $(data);
                    var fieldsGet = '';

                    var KeyDHead = jqueryXML.find("Table").length;
                    for (var x = 1; x < parseInt(KeyDHead) ; x++) {

                        if (entityTbl != "tblHelper") {
                            colDataType.push(jqueryXML.find("Table").eq(x).find('colAlias').eq(0).text());

                        }

                    }
                    //for (var i = 0; i < colDataType.length; i++) {
                    //    alert(colDataType[i]);
                    //}
                    for (var i = 1; i < parseInt(KeyDHead) ; i++) {

                        var rowData = jqueryXML.find('Table').eq(0).children();
                        var textStr = "";
                        var equiValue = "";
                        if (rowData.eq((i)).text() != null) {
                            textStr = rowData.eq((i)).text();
                        }
                        if (entityTbl == "tblHelper") {

                            var selectory = jqueryXML.find('Table').each(function () {
                                //alert(rowData.eq((i)).get(0).tagName);
                                if ($(this).find('colName').text() == rowData.eq((i)).get(0).tagName) {

                                    //equiValue =columns name
                                    equiValue = $(this).find('colAlias').text();
                                }
                            });

                            //alert(equiValue);

                        }
                        if (entityTbl != "tblHelper") {
                            equiValue = colDataType[i - 1];
                        }
                        var fieldys = jqueryXML.find('Table').eq(i).find("FieldType").eq(0).text();
                        if (fieldys == "textarea") {
                            var firstPart = '<div class="fieldy col-md-' + jqueryXML.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="col-md-12"><div class="form-group"><label class="bmd-label-floating">' + jqueryXML.find('Table').eq(i).find("colAlias").eq(0).text();

                            var secondPart = '</label>' +
                                '<textarea   class="form-control" colType="' + jqueryXML.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '" for="' +
                                 jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '"  rows="5">' + textStr + '</textarea></div>' +
                                 '</div></div>';

                            fieldsGet += firstPart + secondPart;
                            alCols.push(jqueryXML.find('Table').eq(i).find("colName").eq(0).text());
                        }
                        if (fieldys == "image") {
                            var firstPart = '<div class="fieldy col-md-' + jqueryXML.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="col-md-12"><div class="form-group"><label class="bmd-label-floating">' + jqueryXML.find('Table').eq(i).find("colAlias").eq(0).text();
                            //< />
                            var secondPart = '</label> <br />' +
                                '<input type="file"  style="width: 100% ; display: -webkit-inline-box;" class="form-control fileUp" colType="' + jqueryXML.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '" for="' +
                                 jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '" value="' + textStr + '"  /> <br />' +
                                 //<img src="uploads/1.jpg" class="img-responsive" style="height:80px"/>
                                 '<img src="uploads/' + textStr + '"  class="img-responsive" style="height:80px"/>' +
                                 '</div></div></div>';

                            fieldsGet += firstPart + secondPart;
                            alCols.push(jqueryXML.find('Table').eq(i).find("colName").eq(0).text());
                        }
                        if (fieldys == "select") {
                            var selectID = jqueryXML.find('Table').eq(i).find("colName").eq(0).text();

                            //var list = $('#relationHdn').load('app.aspx?Entity=' + entityTbl + ' #sl' + selectID).html();
                            var list = $('#sl' + selectID).html();
                            var firstPart = '<div class="fieldy col-md-' + jqueryXML.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="col-md-12"><div class="form-group"><label class="bmd-label-floating">' + jqueryXML.find('Table').eq(i).find("colAlias").eq(0).text();

                            var secondPart = '</label>' +
                                '<select   class="form-control" colType="' + jqueryXML.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '" for="' +
                                 jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '">' +
                                list
                                + '</select>' +
                                '</div></div></div>';
                            fieldsGet += firstPart + secondPart;
                            alCols.push(jqueryXML.find('Table').eq(i).find("colName").eq(0).text());
                            arrRelations.push(jqueryXML.find('Table').eq(i).find("colName").eq(0).text());
                        }
                        if ((fieldys == "number") || (fieldys == "text")) {
                            var firstPart = '<div class="fieldy col-md-' + jqueryXML.find('Table').eq(i).find("colWidth").eq(0).text() + '"><div class="col-md-12"><div class="form-group"><label class="bmd-label-floating">' + jqueryXML.find('Table').eq(i).find("colAlias").eq(0).text();

                            var secondPart = '</label>' +
                                '<input type="text" value="' + textStr + '" class="form-control"  colType="' + jqueryXML.find('Table').eq(i).find("FieldType").eq(0).text() + '" id="txt' + jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '" for="' + jqueryXML.find('Table').eq(i).find("colName").eq(0).text() + '"/>' +
                                '</div></div></div>';

                            fieldsGet += firstPart + secondPart;
                            alCols.push(jqueryXML.find('Table').eq(i).find("colName").eq(0).text());
                        }


                    }
                    $('#pnlGeneric')
                           .prepend($('<div>').append(fieldsGet).addClass("contFields"));

                    for (var xz = 0; xz < arrRelations.length; xz++) {
                        $('#txt' + arrRelations[xz]).val(jqueryXML.find('Table').eq(0).find(arrRelations[xz]).eq(0).text());
                    }

                    ////////////////////////// Replace DDL //////////////////////
                    //replaceDDL();
                },
                error: function (err) {
                    //alert("error in retreiving Entity Data! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('kindly choose entity to manage');
                }
            });
            ajaxDone.done(function () {
                var availableTags = [
          "int",
          "decimal",
          "char(n)",
          "varchar(n)",
          "varchar(max)",
          "text",
          "nchar",
          "nvarchar",
          "nvarchar(max)",
          "ntext",
          "binary",
          "varbinary",
          "varbinary(max)",
          "image",
          "bit"
                ];
                var txtDataType = $('#txtDataType');
                var txtDataTypeSelected = $('#txtDataType').val().toLowerCase();
                //alert(txtDataTypeSelected);
                var parent = $(txtDataType).closest('.form-group');
                $('#txtDataType').remove();
                var options = "";
                for (var i = 0; i < availableTags.length; i++) {
                    if (availableTags[i] == txtDataTypeSelected) {
                        options += "<option selected='selected' value='" + availableTags[i] + "'>" + availableTags[i] + "</option>";
                    }
                    else {
                        options += "<option value='" + availableTags[i] + "'>" + availableTags[i] + "</option>";
                    }

                }
                var select = "<select class='form-control' style='border-radius: 5px; padding: 5px; margin-right: 2%;'>" + options + "</select>";

                //alert(select);
                $(parent).append(select);

                ////////////////////////////////////////////////////
                var FTypes = [
"text",
"select",
"button",
"textarea",
"file",
"date",
"checkbox",
"number",
"password",
"hidden",
"image",
"radio",
"checkbox",
"color",
"datetime-local",
"email",
"month",
"range",
"reset",
"search",
"submit",
"tel",
"time",
"url",
"week"
                ];
                var txtTypeField = $('#txtFieldType');
                var txtTypeFieldSelected = $('#txtFieldType').val().toLowerCase();
                //alert(txtTypeFieldSelected);
                var parent = $(txtTypeField).closest('.form-group');
                $('#txtFieldType').remove();
                var options = "";
                for (var i = 0; i < FTypes.length; i++) {
                    if (FTypes[i] == txtTypeFieldSelected) {
                        options += "<option selected='selected' value='" + FTypes[i] + "'>" + FTypes[i] + "</option>";
                    }
                    else {
                        options += "<option value='" + FTypes[i] + "'>" + FTypes[i] + "</option>";
                    }

                }
                var select = "<select class='form-control' style='border-radius: 5px; padding: 5px; margin-right: 2%;'>" + options + "</select>";
                //alert(select);
                $(parent).append(select);

                ////////////////////////////////////////////////////
                var FOrder = [
"First",
"Second"
                ];
                var txtOrder = $('#txtpriorityView');
                var txtOrderSelected = $('#txtpriorityView').val();
                //alert(txtOrderSelected);
                var parent = $(txtOrder).closest('.form-group');
                $('#txtpriorityView').remove();
                var options = "";
                for (var i = 0; i < FOrder.length; i++) {
                    if (FOrder[i] == txtOrderSelected) {
                        options += "<option selected='selected' value='" + FOrder[i] + "'>" + FOrder[i] + "</option>";
                    }
                    else {
                        options += "<option value='" + FOrder[i] + "'>" + FOrder[i] + "</option>";
                    }

                }
                var select = "<select class='form-control' style='border-radius: 5px; padding: 5px; margin-right: 2%;'>" + options + "</select>";
                //alert(select);
                $(parent).append(select);

                ////////////////////////////////////////////////////
                var FView = [
"YES",
"NO"
                ];
                var txtView = $('#txtenabledView');
                var txtViewSelected = $('#txtenabledView').val();
                //alert(txtViewSelected);
                var parent = $(txtView).closest('.form-group');
                $('#txtenabledView').remove();
                var options = "";
                for (var i = 0; i < FView.length; i++) {
                    if (FView[i] == txtViewSelected) {
                        options += "<option selected='selected' value='" + FView[i] + "'>" + FView[i] + "</option>";
                    }
                    else {
                        options += "<option value='" + FView[i] + "'>" + FView[i] + "</option>";
                    }

                }
                var select = "<select class='form-control' style='border-radius: 5px; padding: 5px; margin-right: 2%;'>" + options + "</select>";
                //alert(select);
                $(parent).append(select);
            });

            //////////////////////////////////// REPLACE DDL ////////////////////////////////
            function replaceDDL() {
                var availableTags = [
          "int",
          "decimal",
          "char(n)",
          "varchar(n)",
          "varchar(max)",
          "text",
          "nchar",
          "nvarchar",
          "nvarchar(max)",
          "ntext",
          "binary",
          "varbinary",
          "varbinary(max)",
          "image",
          "bit"
                ];
                var txtDataType = $('#txtDataType');
                var txtDataTypeSelected = $('#txtDataType').val().toLowerCase();
                //alert(txtDataTypeSelected);
                var parent = $(txtDataType).closest('.form-group');
                $('#txtDataType').remove();
                var options = "";
                for (var i = 0; i < availableTags.length; i++) {
                    if (availableTags[i] == txtDataTypeSelected) {
                        options += "<option selected='selected' value='" + availableTags[i] + "'>" + availableTags[i] + "</option>";
                    }
                    else {
                        options += "<option value='" + availableTags[i] + "'>" + availableTags[i] + "</option>";
                    }

                }
                var select = "<select class='form-control' style='border-radius: 5px; padding: 5px; margin-right: 2%;'>" + options + "</select>";

                //alert(select);
                $(parent).append(select);

                ////////////////////////////////////////////////////
                var FTypes = [
"text",
"select",
"button",
"textarea",
"file",
"date",
"checkbox",
"number",
"password",
"hidden",
"image",
"radio",
"checkbox",
"color",
"datetime-local",
"email",
"month",
"range",
"reset",
"search",
"submit",
"tel",
"time",
"url",
"week"
                ];
                var txtTypeField = $('#txtFieldType');
                var txtTypeFieldSelected = $('#txtFieldType').val().toLowerCase();
                //alert(txtTypeFieldSelected);
                var parent = $(txtTypeField).closest('.form-group');
                $('#txtFieldType').remove();
                var options = "";
                for (var i = 0; i < FTypes.length; i++) {
                    if (FTypes[i] == txtTypeFieldSelected) {
                        options += "<option selected='selected' value='" + FTypes[i] + "'>" + FTypes[i] + "</option>";
                    }
                    else {
                        options += "<option value='" + FTypes[i] + "'>" + FTypes[i] + "</option>";
                    }

                }
                var select = "<select class='form-control' style='border-radius: 5px; padding: 5px; margin-right: 2%;'>" + options + "</select>";
                //alert(select);
                $(parent).append(select);

                ////////////////////////////////////////////////////
                var FOrder = [
"First",
"Second"
                ];
                var txtOrder = $('#txtpriorityView');
                var txtOrderSelected = $('#txtpriorityView').val();
                //alert(txtOrderSelected);
                var parent = $(txtOrder).closest('.form-group');
                $('#txtpriorityView').remove();
                var options = "";
                for (var i = 0; i < FOrder.length; i++) {
                    if (FOrder[i] == txtOrderSelected) {
                        options += "<option selected='selected' value='" + FOrder[i] + "'>" + FOrder[i] + "</option>";
                    }
                    else {
                        options += "<option value='" + FOrder[i] + "'>" + FOrder[i] + "</option>";
                    }

                }
                var select = "<select class='form-control' style='border-radius: 5px; padding: 5px; margin-right: 2%;'>" + options + "</select>";
                //alert(select);
                $(parent).append(select);

                ////////////////////////////////////////////////////
                var FView = [
"YES",
"NO"
                ];
                var txtView = $('#txtenabledView');
                var txtViewSelected = $('#txtenabledView').val();
                //alert(txtViewSelected);
                var parent = $(txtView).closest('.form-group');
                $('#txtenabledView').remove();
                var options = "";
                for (var i = 0; i < FView.length; i++) {
                    if (FView[i] == txtViewSelected) {
                        options += "<option selected='selected' value='" + FView[i] + "'>" + FView[i] + "</option>";
                    }
                    else {
                        options += "<option value='" + FView[i] + "'>" + FView[i] + "</option>";
                    }

                }
                var select = "<select class='form-control' style='border-radius: 5px; padding: 5px; margin-right: 2%;'>" + options + "</select>";
                //alert(select);
                $(parent).append(select);
            }

            //--------------------------------------------------------------------------------
            // get updated data and send it to database
            $("#btnUpdate").click(function () {
                var ID = GetParameterValues('ID');
                var entityTbl = GetParameterValues('Entity');
                var fieldss = [];
                var valuess = [];
                var tblColType = [];
                $(".contFields input[type=text]").each(function () {
                    var field = $(this).attr('for');
                    var value = $(this).val();
                    fieldss.push(field);
                    valuess.push(value);
                });
                $(".contFields textarea").each(function () {
                    var field = $(this).attr('for');
                    var value = $(this).val();
                    fieldss.push(field);
                    valuess.push(value);
                });
                $(".contFields select").each(function () {
                    var field = $(this).attr('for');
                    var value = $(this).val();
                    fieldss.push(field);
                    valuess.push(value);
                });
                $(".contFields input[type=file]").each(function () {
                    var field = $(this).attr('for');
                    var value = $(this).attr('value');
                    //alert(value);
                    fieldss.push(field);
                    //alert($(".fileUp").get(0).files.length);
                    if ($(".fileUp").get(0).files.length == 0) {
                        valuess.push(value);
                        var strUpdate = "";

                        for (var y = 0; y < fieldss.length; y++) {

                            if (valuess[y] != null) {
                                strUpdate += " " + fieldss[y] + " = N'" + valuess[y] + "'";
                            }
                            if (valuess[y] == null) {
                                strUpdate += " " + fieldss[y] + " = NULL ";
                            }
                            if (y < (fieldss.length - 1)) {
                                strUpdate += " , "
                            }
                        }

                        var updateAjax = $.ajax({
                            url: 'Service.asmx/updateGen',
                            method: 'POST',
                            contentType: 'application/json;charset-utf-8',
                            data: JSON.stringify({ tblName: entityTbl, idUp: ID, txt: strUpdate }),
                            dataType: 'json',
                            success: function (data) {

                                $('.myAlert.success').css('display', 'block');
                                $('#succMsg').html('data succefuly edited');
                            },
                            error: function (err) {
                                $('.myAlert.failed').css('display', 'block');
                                $('#failMsg').html('failed to Edited data, please try again');
                            }
                        });
                    }
                    else {

                        var data = new FormData();
                        var files = $(".fileUp").get(0).files;
                        data.append("UploadedImage", files[0]);
                        var ajaxRequest = $.ajax({
                            type: "POST",
                            url: "Service.asmx/UploadFile",
                            contentType: false,
                            processData: false,
                            data: data,
                            success: function (data) {
                                var jqueryy = $(data);
                                var newName = $(jqueryy).find('string').text();
                                valuess.push(newName);
                            }
                        });
                        ajaxRequest.done(function (xhr, textStatus) {

                            var strUpdate = "";

                            for (var y = 0; y < fieldss.length; y++) {

                                if (valuess[y] != null) {
                                    strUpdate += " " + fieldss[y] + " = N'" + valuess[y] + "'";
                                }
                                if (valuess[y] == null) {
                                    strUpdate += " " + fieldss[y] + " = NULL ";
                                }
                                if (y < (fieldss.length - 1)) {
                                    strUpdate += " , "
                                }
                            }

                            var updateAjax = $.ajax({
                                url: 'Service.asmx/updateGen',
                                method: 'POST',
                                contentType: 'application/json;charset-utf-8',
                                data: JSON.stringify({ tblName: entityTbl, idUp: ID, txt: strUpdate }),
                                dataType: 'json',
                                success: function (data) {

                                    $('.myAlert.success').css('display', 'block');
                                    $('#succMsg').html('data succefuly edited');
                                },
                                error: function (err) {
                                    $('.myAlert.failed').css('display', 'block');
                                    $('#failMsg').html('failed to Edited data, please try again');
                                }
                            });
                        });
                    }



                });

                for (var i = 0; i < fieldss.length; i++) {
                    if ($("#txt" + fieldss[i]).attr('colType') != 'image') {
                        tblColType.push($("#txt" + fieldss[i]).attr('colType'));
                    }
                }

                if (count(tblColType) == 0) {
                    var strUpdate = "";

                    for (var y = 0; y < fieldss.length; y++) {

                        if (valuess[y] != null) {
                            strUpdate += " " + fieldss[y] + " = N'" + valuess[y] + "'";
                        }
                        if (valuess[y] == null) {
                            strUpdate += " " + fieldss[y] + " = NULL ";
                        }
                        if (y < (fieldss.length - 1)) {
                            strUpdate += " , "
                        }
                    }

                    var updateAjax = $.ajax({
                        url: 'Service.asmx/updateGen',
                        method: 'POST',
                        contentType: 'application/json;charset-utf-8',
                        data: JSON.stringify({ tblName: entityTbl, idUp: ID, txt: strUpdate }),
                        dataType: 'json',
                        success: function (data) {

                            $('.myAlert.success').css('display', 'block');
                            $('#succMsg').html('data succefuly edited');
                        },
                        error: function (err) {
                            $('.myAlert.failed').css('display', 'block');
                            $('#failMsg').html('failed to Edited data, please try again');
                        }
                    });
                }


                return false;
            });

        });

        // count fields that is of image type
        function count(myArrarr) {
            array_elements = myArrarr;


            var current = null;
            var cnt = 0;
            for (var i = 0; i < array_elements.length; i++) {
                if (array_elements[i] == 'image') {
                    cnt++;
                }
            }
            return cnt;

        }

    </script>

</asp:Content>
