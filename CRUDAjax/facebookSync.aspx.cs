﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Facebook;


namespace CRUDAjax
{
    public partial class facebookSync : System.Web.UI.Page
    {
        //private const string ExtendedPermissions = "user_about_me,read_stream,publish_stream";

        protected void Page_Load(object sender, EventArgs e)
        {

            //postItNow();
            addPost();

        }
        protected void addPost() {
            FacebookClient fb = new FacebookClient("EAAHaf6UOeH8BAAPVU3VWlD31aUcgbOwDcZAP8but2b80j4Liwf5Rb6zwEIEABcn46LwqALZCLr4tpJ724dLEvSv9ixLLO4nZA5QZChUZCLaM7dvZAmVnBcZBpzWbAZBFuNQJYZCRQUHvl5Q2yHhtC8Yp8JIJ91OJYeccoIZCse0PLp8gZDZD");

            fb.PostCompleted += (o, e) => {
                if (e.Error == null)
                {
                    var result = (IDictionary<string, object>)e.GetResultData();
                    //var newPostId = (string)result.id;
                }
            };

            var parameters = new Dictionary<string, object>();
            parameters["message"] = "My first wall post using Facebook SDK for .NET";
            fb.PostAsync("me/feed", parameters);
        }
        protected void postItNow()
        {
            post2Face("460590547760073", "hello from CRUD using backend", "521716741601407", "f65fd7f3643a360f0c06b28872815aa9", "EAAHaf6UOeH8BAAPVU3VWlD31aUcgbOwDcZAP8but2b80j4Liwf5Rb6zwEIEABcn46LwqALZCLr4tpJ724dLEvSv9ixLLO4nZA5QZChUZCLaM7dvZAmVnBcZBpzWbAZBFuNQJYZCRQUHvl5Q2yHhtC8Yp8JIJ91OJYeccoIZCse0PLp8gZDZD");
        }
        protected void post2Face(string pgID, string msg, string AppID, string AppSec, string AccToken)
        {
            FacebookClient clientX = checkAuthorization(AppID, AppSec, AccToken);

            //clientX.Post("/" + pgID + "/feed", new { message = msg });
            clientX.Post("/" + pgID + "/photos", new { published = "false", caption = "image by C#", url = "https://c.tadst.com/gfx/750w/doomsday-rule.jpg?1" });
            //clientX.Post("/" + pgID + "/feed", new { message = "this photo is published using C#", attached_media[0] = "{ 'media_fbid' : '460590611093400' }", attached_media[1] = "{ 'media_fbid' : '460590611093400' }", caption = "this is a trial caption" });
        }
        protected FacebookClient checkAuthorization(string appID, string appSec, string accToken)
        {
            string app_id = appID;
            string app_secret = appSec;
            string scope = "publish_to_groups, publish_pages, manage_pages";
            FacebookClient client = new FacebookClient();

            if (Request["code"] == null)
            {
                Response.Redirect(string.Format("https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope={2}",
                    app_id, Request.Url.AbsoluteUri, scope
                    ));
            }
            else
            {
                string url = string.Format("https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&scope={2}&code={3}&client_secret={4}",
                    app_id, Request.Url.AbsoluteUri, scope, Request["code"].ToString(), app_secret
                    );
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                string access_token = accToken;

                client = new FacebookClient(access_token);
            }
            return client;
        }

    }
}