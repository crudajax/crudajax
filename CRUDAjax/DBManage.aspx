﻿<%@ Page Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="DBManage.aspx.cs" Inherits="CRUDAjax.DBManage" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
    Manage Database
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="assetsPro/css/material-dashboard.min40a0.css?v=2.0.2" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <style>
        .cont {
            /*rgb(145,40,172)*/
            /*margin-top: 90px;*/
            margin: 90px 20px 0 20px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }

        body .selectpicker {
            background-color: #9c27b0;
            /* border-radius: 25px; */
            border-radius: 5px;
            color: white;
            line-height: 25pt;
            width: auto;
            text-align-last: center;
            height: 50px;
            /*font-size: large !important;*/
            padding-bottom: 5px !important;
            min-width: 20%;
        }

        .myOption {
            background-color: white;
            color: #555;
            text-align-last: center;
            border-radius: 5px;
        }

            .myOption:hover {
                background-color: #9c27b0 !important;
                color: white !important;
            }

        .butn {
            background-color: #9c27b0;
            color: white;
            cursor: pointer;
            margin-left: 25px;
            border-radius: 5px;
        }

        .txt {
            border-radius: 5px;
            padding: 8px;
        }

        .icon {
            font-size: 30px;
            color: #9c27b0;
            cursor: pointer;
        }

        body .myAlert {
            width: 80%;
            z-index: 8000;
            margin: auto;
            display: none;
        }

        .newcol {
            border-bottom: 2px #808080 solid;
            padding-bottom: 10px;
            margin-bottom: 15px;
        }

            .newcol:last-of-type {
                border-bottom: none;
            }
    </style>
    <%--<script>
        $(document).ready(function () {
            var availableTags = [
              "char(n)",
              "varchar(n)",
              "varchar(max)",
              "text",
              "nchar",
              "nvarchar",
              "nvarchar(max)",
              "ntext",
              "binary",
              "varbinary",
              "varbinary(max)",
              "image",
            ];
            $(".txtDataType").autocomplete({
                source: availableTags
            });
        });
    </script>--%>

    <script>
        $(window).load(function () {
            var accountTYPE = '<%= Session["accountType"] %>';
            //alert(accountTYPE);
            if (accountTYPE != "Super Admin") {
                if (accountTYPE != "admin") {
                    $(location).attr("href", "Default.aspx");
                }
                
            }
        });
        
    </script>
    <script>
        $(document).ready(function () {
            //get field types as database types and input types
            var availableTags = [
              "int",
              "decimal",
              "char(n)",
              "varchar(n)",
              "varchar(max)",
              "text",
              "nchar",
              "nvarchar",
              "nvarchar(max)",
              "ntext",
              "binary",
              "varbinary",
              "varbinary(max)",
              "image",
              "bit"
            ];
            var FTypes = [
                "text",
                "select",
                "button",
                "textarea",
                "file",
                "date",
                "checkbox",
                "number",
                "password",
                "hidden",
                "image",
                "radio",
                "checkbox",
                "color",
                "datetime-local",
                "email",
                "month",
                "range",
                "reset",
                "search",
                "submit",
                "tel",
                "time",
                "url",
                "week"

            ];
            //ddlFT
            for (var i = 0; i < availableTags.length; i++) {
                $("#ddlTest").append(
                    $("<option>").attr("value", availableTags[i].toString()).html(availableTags[i].toString())
                    );
            }

            for (var x = 0; x < FTypes.length; x++) {
                $("#ddlFT").append(
                    $("<option>").attr("value", FTypes[x].toString()).html(FTypes[x].toString())
                );
                $("#selectFT").append(
                    $("<option>").attr("value", FTypes[x].toString()).html(FTypes[x].toString())
                );
            }
        });


    </script>
    <style>
        body.dragging, body.dragging * {
            cursor: move !important;
        }

        ol {
            list-style-type: none;
        }

        .dragged {
            position: absolute;
            opacity: 0.5;
            z-index: 2000;
        }

        ol.example li.placeholder {
            position: relative;
            /** More li styles **/
        }

            ol.example li.placeholder:before {
                position: absolute;
                /** Define arrowhead **/
            }

        .margined li {
            margin: 10px auto;
        }

        .tblHeaderMng {
            display: none;
        }

        .HeaderMng .lead {
            margin-bottom: 0px;
        }

        .myAlert .close {
            position: absolute;
            top: 5px;
            right: 5px;
        }

        .addCols .form-control:last-child {
            background: none !important;
        }

        ol.margined.tblattrs.col-xl-12 button {
            font-family: 'Aclonica';
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cont">
        <div class="alert alert-success fade show myAlert success">
            <strong>Good News!</strong> <span id="succMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close">
                <span class="hideAlert">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger  fade show myAlert failed">
            <strong>Sorry!</strong> <span id="failMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close" aria-label="Close">
                <span class="hideAlert">&times;</span>
            </button>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12" style="font-family: 'Aclonica';">
            <select class="selectpicker" data-size="7" title="Single Select">
                <option class="myOptions" value="0" disabled="disabled" selected="selected">DB Tables</option>
            </select>
            <%-- <button type="button" class="butn btn-sm" onclick="if(confirm('Do you really want to submit the form?')) delTable(); ">Delete This Table</button>--%>
            <button type="button" class="butn btn-sm" data-toggle="modal" data-target='#myModal3'>Delete This Table</button>
            <button type="button" class="butn btn-sm" data-toggle="modal" data-target='#myModal2'>Insert Table</button>
            <%--<button type="button" class="butn btn-sm"></button>--%>
            <select class="butn btn-sm" id="selectPerm"></select>

        </div>


        <div id="retreive" class="tab-pane fade active show">
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title "><span class="entitySpan"></span>
                                        Table Columns
                                    </h4>
                                    <p class="card-category">Here is a subtitle for this Table</p>
                                </div>
                                <div class="card-body">
                                    <ol class="margined tblHeaderMng col-xl-12" style="margin: 0 auto;">
                                        <li style="margin: 0px auto; width: 100%;">
                                            <div class="HeaderMng" style="display: -webkit-inline-box; width: 100%;">
                                                <p class="lead" style="width: 15%; margin-right: 1%;">Column Name</p>
                                                <p class="lead" style="width: 15%; margin-right: 2%;">Table Related</p>
                                                <p class="lead" style="width: 15%; margin-right: 2%;">Related Value</p>
                                                <p class="lead" style="width: 15%; margin-right: 2%;">Data Type</p>
                                                <p class="lead" style="width: 15%; margin-right: 2%;">Field Type</p>
                                                <p class="lead" style="min-width: 45px; width: 15%">Edit</p>
                                            </div>
                                        </li>
                                    </ol>
                                    <ol class="margined tblattrs col-xl-12">
                                        <li>
                                            <p class="lead">Choose Table You Want To Manage</p>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <select id="ddlTest" style="display: none;"></select>
    <select id="ddlFT" style="display: none;"></select>


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-center">Enter New Column Info</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group addCols">
                                <input type="text" id="txtColNme" class="form-control" placeholder="Column Name" />
                                <select class="form-control" id="selectDT">
                                    <option disabled="disabled" selected="selected">Data Type</option>
                                    <option>int</option>
                                    <option>decimal</option>
                                    <option>char(n)</option>
                                    <option>varchar(n)</option>
                                    <option>varchar(max)</option>
                                    <option>text</option>
                                    <option>nchar</option>
                                    <option>nvarchar</option>
                                    <option>nvarchar(max)</option>
                                    <option>ntext</option>
                                    <option>binary</option>
                                    <option>varbinary</option>
                                    <option>varbinary(max)</option>
                                    <option>image</option>
                                    <option>bit</option>
                                </select>
                                <input type="text" id="txtColAlias" class="form-control" placeholder="Column Alternative" />

                                <%--<div class="form-control">
                                        <input type="checkbox" value="Primary" id="Primary" />
                                        Is Primary
                                    </div>--%>
                                <div class="form-control">
                                    <input type="checkbox" value="Null" id="Null" />
                                    Is Null
                                </div>
                                <div class="form-control">
                                    <input type="checkbox" value="Identity" id="Identity" />
                                    Is Identity
                                </div>
                                <%--field priority view--%>
                                <select class="form-control" id="selectPV">
                                    <option disabled="disabled" selected="selected">View Order</option>
                                    <option value="First">First</option>
                                    <option value="Second">Second</option>
                                </select>
                                <%--field enabled view--%>
                                <select class="form-control" id="selectEdit">
                                    <option disabled="disabled" selected="selected">Editable</option>
                                    <option value="YES">YES</option>
                                    <option value="NO">NO</option>
                                </select>
                                <%--field Field Type--%>
                                <select class="form-control" id="selectFT">
                                    <option disabled="disabled" selected="selected">Field Type</option>
                                </select>
                                <input type="text" id="txtColWidth" class="form-control" placeholder="Field Width" />


                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnInsertCol" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal 2 -->

    <!-- Modal 3 -->
    <div class="modal fade" id="myModal3" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-center">Delete Table</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div>
                            <p class="col-md-12 lead">
                                Do You Sure Want To Submit Deleting This Table ?
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnDeleteTbl" class="btn btn-success" onclick="delTable()" data-dismiss="modal">Confirm Delete</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>




    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-center">Enter New Table Info</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div>
                            <div class="col-md-12">
                                <input type="text" id="txtTblNme" class="txt col-xl-9 border-light" placeholder="Table Name" />
                                <%-- Add Icon --%>
                                <div onclick="addCol();" class="col-xl-2" style="display: none; width: 50px;"><i class="icon fa fa-plus-circle" data-toggle="tooltip" title="Add Column"></i></div>
                                <%--<hr />--%><br />
                                <br />

                                <div id="colAdd" style="display: none;">
                                    <%--<div class="col-xl-10">
                                        <input type="text" id="" class="txt col-xl-6" placeholder="Column Name" />
                                        <span class="col-xl-6">
                                            <input type="checkbox" value="Null" id="" />
                                            Is Null
                                        </span>

                                        <select class="txt col-xl-6" id="">
                                            <option disabled="disabled" selected="selected">Data Type</option>
                                            <option>int</option>
                                            <option>char(n)</option>
                                            <option>varchar(n)</option>
                                            <option>varchar(max)</option>
                                            <option>text</option>
                                            <option>nchar</option>
                                            <option>nvarchar</option>
                                            <option>nvarchar(max)</option>
                                            <option>ntext</option>
                                            <option>binary</option>
                                            <option>varbinary</option>
                                            <option>varbinary(max)</option>
                                            <option>image</option>
                                        </select>
                                        <span class="col-xl-4">
                                            <input type="checkbox" value="Identity" id="" />
                                            Is Identity
                                        </span>
                                        <span class="col-xl-2">
                                            <i class="fa fa-trash" style="font-size: 24px; color: red;cursor: pointer;"></i>
                                        </span>
                                    </div>--%>
                                    <%--<hr />--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnInsertTbl" class="btn btn-success" onclick="createTbl()">Create Table</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            //fill drop down list with table names
            selectDBTables();
            loadAccLevels();
            $('[data-toggle="tooltip"]').tooltip();
            $('.hideAlert').click(function hideAlert() {
                $(this).closest('.myAlert').css('display', 'none');
            });

        });

        var accessLevelsName = new Array();
        var accessLevelsID = new Array();

        function loadAccLevels() {
            var ajaxreq = $.ajax({
                url: 'Service.asmx/selectGen',
                method: 'post',
                data: { tblNameGen: "AccessLevel" },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    //count of rows data
                    var KeyD = jqueryXML.find("Table").length;
                    //alert(KeyD);
                    var options = "";
                    options += "<option disabled='disabled' selected='selected'>Access Name</option>"
                    for (var i = 0; i < KeyD; i++) {

                        accessLevelsName.push(jqueryXML.find("Table").eq(i).find("accessName").text());
                        accessLevelsID.push(jqueryXML.find("Table").eq(i).find("ID").text());

                        //alert(jqueryXML.find("Table").eq(i).find("accessName").text());
                        options += "<option value='" + jqueryXML.find("Table").eq(i).find("ID").text() + "'>" + jqueryXML.find("Table").eq(i).find("accessName").text() + "</option>";
                    }
                    //alert(options);
                    $('#selectPerm').append(options);
                },
                error: function (err) {
                    //alert("Error");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to retrieve data general, please try again');
                }
            });
        }
        // count how many columns in this table
        function elemCount() {
            var maximum = 0;
            $('.newcol').each(function (i, elem) {
                var justValue = parseInt($(elem).attr('ids')) + parseInt(1);
                maximum = (justValue > maximum) ? justValue : maximum;
            });

            return maximum;
        }
        // delete database column from table
        function deleteColRow(rowID) {
            //alert("delete");
            var containerRow = $("[ids='" + rowID + "']");
            $(containerRow).remove();
        }
        //create table
        function createTbl() {

            var tblname = $("#txtTblNme").val();


            var coldatas = " ID int NOT NULL IDENTITY(1,1) PRIMARY KEY ";
            var coldataType = "";
            var count = 1;
            //txtColAlias

            if (tblname == "") {
                $('.myAlert.failed').css('display', 'block');
                $('#failMsg').html('table name must be filled, please enter table name ');
            }
            else {

                //$(".newcol").each(function (i, elem) {
                //    if ($(elem).find('.txtColName').val() != "" && $(elem).find('.ddlColType option:selected').val())
                //    {
                //        var colNull = " NOT NULL ";
                //        var colIdentity = " ";
                //        if ($(elem).find('.chckNull').is(':checked')) {
                //            colNull = " ";
                //        }
                //        if ($(elem).find('.chckIdentity').is(':checked')) {
                //            colIdentity = " IDENTITY(1,1) ";
                //        }
                //        var coldata = " " + $(elem).find('.txtColName').val() + " " + $(elem).find('.ddlColType option:selected').val() + " " + colNull + colIdentity;
                //        coldataType = "'" + $("#txtTblNme").val() + "','" + $(elem).find('.txtColName').val() + "','" + $(elem).find('.txtColAlias').val() + "','" + $(elem).find('.ddlColType option:selected').val() + "'";
                //        count++;
                //    }
                //    if (count > 0) {
                //        $.ajax({
                //            url: 'Service.asmx/inserTblHelper',
                //            method: 'POST',
                //            contentType: 'application/json;charset-utf-8',
                //            data: JSON.stringify({ cols: 'tblName,colName,colAlias,DataType', txt: coldataType }),
                //            dataType: 'json',
                //            success: function (data) {
                //                //alert('added')
                //                $('.myAlert.success').css('display', 'block');
                //                $('#succMsg').html('added successfully to the helper table');
                //            },
                //            error: function (err) {
                //                //alert('error')
                //                $('.myAlert.failed').css('display', 'block');
                //                $('#failMsg').html('failed to add data to the helper table, please try again');
                //            }
                //        });
                //        coldatas += coldata + " , ";
                //    }
                //});
                //coldatas = coldatas.slice(0, -2);
                //alert(count);
                // Columns of a new table
                //alert(coldatas);
                if (count > 0) {
                    $.ajax({
                        url: 'Service.asmx/createTbl',
                        method: 'POST',
                        contentType: 'application/json;charset-utf-8',
                        data: JSON.stringify({ tblName: tblname, colData: coldatas }),
                        dataType: 'json',
                        success: function (data) {
                            AddAllPermToTable(tblname);
                            //alert("added Succefully!");
                            selectDBTables();
                            $(".selectpicker").val($("#txtTblNme").val());
                            fillTblFields();
                        },
                        error: function (err) {
                            $('.myAlert.failed').css('display', 'block');
                            $('#failMsg').html('failed in adding table, please try again');
                        }
                    });

                }
                else {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('table must Contain at least 1 field');
                }
            }



            return false;
        }
        //add fields of new column to be filled
        function addCol() {
            //alert("clicked");
            var newRowCol = parseInt(elemCount());


            var fent = "<div class='col-xl-12 newcol' ids='" + newRowCol + "'><input type='text' id='' class='txt col-xl-6 txtColName' placeholder='Column Name' /><span class='col-xl-6'><input type='checkbox' class='chckNull' value='Null' id='' />Is Null</span><select class='txt col-xl-6 ddlColType' id=''><option disabled='disabled' selected='selected'>Data Type</option><option>int</option><option>decimal</option><option>char(n)</option><option>varchar(n)</option><option>varchar(max)</option><option>text</option><option>nchar</option><option>nvarchar</option><option>nvarchar(max)</option><option>ntext</option><option>binary</option><option>varbinary</option><option>varbinary(max)</option><option>image</option></select><span class='col-xl-4'><input type='checkbox' value='Identity' id='' class='chckIdentity' />Is Identity</span><input type='text' id='' class='txt col-xl-10 txtColAlias' placeholder='Alternate Title' /><span class='col-xl-2' onclick='deleteColRow(" + newRowCol + ")'><i class='fa fa-trash' style='font-size: 24px; color: red;cursor: pointer;'></i></span></div></div>";
            $("#colAdd").append(content);
        }
        // delete table
        function delTable() {
            var TblName = $(".selectpicker option:selected").val();

            if (TblName != 0) {
                $.ajax({
                    url: 'Service.asmx/deleteTbl',
                    method: 'POST',
                    contentType: 'application/json;charset-utf-8',
                    data: JSON.stringify({ tblName: TblName }),
                    dataType: 'json',
                    success: function (data) {
                        $('.myAlert.success').css('display', 'block');
                        $('#succMsg').html('Table ' + TblName + ' successfully Deleted');
                        selectDBTables();
                        var innerList = '<li><p class="lead">Choose Table You Want To Manage</p></li>';
                        $('.tblattrs').html('');
                        $('.tblHeaderMng').css('display', 'none');
                        //$('.tblattrs').append($('li').append($('p').html('Choose Table You Want To Manage').addClass('lead')));
                        $('.tblattrs').append(innerList);
                    },
                    error: function (err) {
                        //alert("Error");
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed to delete this table, please try again');
                    }
                });

                $.ajax({
                    url: 'Service.asmx/deleteallTblfrmTblHeader',
                    method: 'POST',
                    contentType: 'application/json;charset-utf-8',
                    data: JSON.stringify({ tblName: TblName }),
                    dataType: 'json',
                    success: function (data) {
                        //location.reload();
                        $('.myAlert.success').css('display', 'block');
                        $('#succMsg').html('data of table ' + TblName + 'successfully Deleted from tblHelper');
                    },
                    error: function (err) {
                        //location.reload();
                        //alert("Error");
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed to data of table ' + TblName + ' from tblHelper please try again');
                    }
                });
            }
            else {
                $('.myAlert.failed').css('display', 'block');
                $('#failMsg').html('Please Choose Table That You Want To Delete');
            }


        }
        // select all table names to fill drop down list
        function selectDBTables() {
            $(".selectpicker").html('');
            $(".selectpicker").append($('<option>').attr("value", 0).html('Select Table'));
            $.ajax({
                url: 'Service.asmx/selectDBTables',
                method: 'post',
                data: {},
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    var KeyD = jqueryXML.find("TABLE_NAME").length;
                    for (var i = 0; i < parseInt(KeyD) ; i++) {
                        var rowData = jqueryXML.find("TABLE_NAME").eq(i).text();
                        $(".selectpicker").append($('<option>').attr("value", rowData).html(rowData));
                    }

                },
                error: function (err) {
                    //alert("error in retrieving Entity! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving Entity, please try again');
                }
            });
            return false;
        }
        // on change event of ddl tables names which means on selecting a table to manage
        $(".selectpicker").change(function () {
            fillTblFields();
        });
        // get table names to fill ddl tables options
        function bound(elem) {
            //alert(elem);
            var conceptName = $(elem).eq(0).find('.DBTblDrop').find(":selected").text();
            var arrayCols = [];
            $.ajax({
                url: 'Service.asmx/getTblCols',
                method: 'post',
                data: { tableName: conceptName },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    var tblsAttr = jqueryXML.find('Table').find("TABLE_SCHEMA:empty").length;
                    //var countx = jqueryXML.find("Table").eq(0).children().length;
                    var KeyDPH = jqueryXML.find('Table').length;
                    var tblNme = $('.selectpicker').find(":selected").text();
                    $(elem).eq(0).find(".tblColDrop").html('');
                    for (var i = 0; i < parseInt(KeyDPH) - parseInt(tblsAttr) ; i++) {
                        arrayCols.push(jqueryXML.find("COLUMN_NAME").eq(i).text());

                        $(elem).eq(0).find(".tblColDrop").append($('<option>').attr("value", jqueryXML.find("COLUMN_NAME").eq(i).text()).text(jqueryXML.find("COLUMN_NAME").eq(i).text()));
                    }

                },
                error: function (err) {
                    //alert("error in retrieving Data! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to retrieve data, please try again');
                }
            });


        }
        // get table columns and it's data 
        function fillTblFields() {

            var NameOFTable = $(".selectpicker option:selected").text();
            $('#dataTable tbody tr').remove();
            $(".tblattrs").html('');
            var optionsLst = $("#ddlTest").html();
            var optionsFT = $("#ddlFT").html();

            $.ajax({
                url: 'Service.asmx/getTblCols',
                method: 'post',
                data: { tableName: NameOFTable },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    // count of only table helper data (field data type, relations and so on)
                    var tblsAttr = jqueryXML.find('Table').find("TABLE_SCHEMA:empty").length;
                    //var countx = jqueryXML.find("Table").eq(0).children().length;
                    // count of all fields in tablehelper and the information schema
                    var KeyDPH = jqueryXML.find('Table').length;
                    var tblNme = $('.selectpicker').find(":selected").text();

                    // loop with length of count(information schema rows [which is columns count in table])
                    for (var i = 0; i < parseInt(KeyDPH) - parseInt(tblsAttr) ; i++) {
                        var fieldName = jqueryXML.find("COLUMN_NAME").eq(i).text();
                        var fieldTyp = jqueryXML.find('colName:contains(' + (fieldName) + ')').closest('Table').find('FieldType').text();
                        var lengthDT = jqueryXML.find("CHARACTER_MAXIMUM_LENGTH").eq(i).text();
                        var dtypy = "";
                        if (lengthDT == "-1") {
                            dtypy = jqueryXML.find("DATA_TYPE").eq(i).text() + "(max)";
                            //alert(dtypy);
                        }
                        else {
                            dtypy = jqueryXML.find("DATA_TYPE").eq(i).text();
                        }
                        //alert(fieldName + "  and " + fieldTyp);
                        $(".tblattrs").append(
                            $("<li>").addClass(jqueryXML.find("COLUMN_NAME").eq(i).text())
                            .append($("<input>").attr('oldColName', jqueryXML.find("COLUMN_NAME").eq(i).text()).attr('class', 'colName').css({ "width": "15%", "marginRight": "1%", "border-radius": "5px", "padding": "5px", "border": "1px solid #A9A9A9" }).attr('placeholder', 'Column Name').val(jqueryXML.find("COLUMN_NAME").eq(i).text()))
                            .append($("<select>").attr('ids', "DBTblDrop" + i).css({ "border-radius": "5px", "padding": "5px", "width": "15%", "marginRight": "2%" }).addClass("col-xl-10 DBTblDrop DBTblDrop" + i).html($('.selectpicker').html()).val(jqueryXML.find("DATA_TYPE").eq(i).text()))
                            .append($("<select>").attr('ids', "tblColDrop" + i).css({ "border-radius": "5px", "padding": "5px", "width": "15%", "marginRight": "2%" }).addClass("col-xl-10 tblColDrop tblColDrop" + i))
                            .append($("<select>").attr('ids', "DTDrop" + i).css({ "border-radius": "5px", "padding": "5px", "width": "15%", "marginRight": "2%" }).addClass("col-xl-10 DTDrop").html($(optionsLst)).val(dtypy))
                            .append($("<select>").attr('ids', "FTDrop" + i).css({ "border-radius": "5px", "padding": "5px", "width": "15%", "marginRight": "2%" }).addClass("col-xl-10 FTDrop FTDrop" + i).html($(optionsFT)).val(fieldTyp))

                            .append($('<div>').attr('ids', "Allow" + jqueryXML.find("COLUMN_NAME").eq(i).text()).attr('style', 'width: 0.5%; min-width: 15px; position: relative; top: 12px;').css('display', 'inline-block').append($('<input>').attr('class', 'form-control allowAtr' + i).attr('type', 'checkbox').css({ "marginRight": "1%", "border-radius": "5px" })))

                            .append($('<div>').attr('ids', "Update" + jqueryXML.find("COLUMN_NAME").eq(i).text()).attr("onclick", "UpdateAttr('" + tblNme + "','" + jqueryXML.find('COLUMN_NAME').eq(i).text() + "');").attr('style', ' min-width: 15px; position: relative; top: 5px;').css('display', 'inline-block').append($('<i>').html('edit').attr('class', 'material-icons').attr('style', "font-size: 24px; color:#89229b ;cursor: pointer;")))
                            .append($('<div>').attr('ids', jqueryXML.find("COLUMN_NAME").eq(i).text()).attr('class', 'btnDelete').attr("onclick", "deleteAttr('" + tblNme + "','" + jqueryXML.find('COLUMN_NAME').eq(i).text() + "');").attr('style', ' min-width: 15px; position: relative; top: 5px;').css('display', 'inline-block').append($('<i>').attr('class', 'fa fa-trash').attr('style', "font-size: 24px; color: red;cursor: pointer;"))));
                        $(".DBTblDrop" + i + " option[value='" + tblNme + "']").remove();
                        $(".DBTblDrop" + i + " option").eq(0).before($("<option>").attr("selected", "selected").val("0").text("Select table"));
                        $('.DBTblDrop' + i).bind('change', function () {
                            var className = "." + $(this).parent('li').prop('className');
                            bound(className);
                        });
                    }

                    $('select.FTDrop0').eq(0).attr('disabled', 'disabled').css('background', '#cecece').val('text');
                    $(".tblattrs")
                        .append("<button type='button' class='butn btn-sm' data-toggle='modal' data-target='#myModal'>Insert Column</button>")
                        .append("<button type='button' class='butn btn-sm' onclick='saveOrder();' >Save Order</button>")
                        .append("<button type='button' onclick='btnperms();' class='butn btn-sm' >Apply Permessions</button>");
                    $(".tblHeaderMng").css('display', 'inline-block');
                    //var btnClicked = $(this);
                },
                error: function (err) {
                    //alert("error in retrieving Data! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to retrieve data, please try again');
                }
            });
            return false;
        }
        // delete column
        function deleteAttr(tableName, AttrName) {

            $.ajax({
                url: 'Service.asmx/deleteCol',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: tableName, colName: AttrName }),
                dataType: 'json',
                success: function (data) {
                    //alert(data.d); 
                    var msg = data.d;
                    if (msg.indexOf("Sorry") >= 0) {
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed to delete this column from ' + tableName + ' table, this column is already in relation as refernce column');
                    }
                    else {
                        $('.myAlert.success').css('display', 'block');
                        $('#succMsg').html(AttrName + 'successfully deleted from ' + tableName + ' table');

                        $.ajax({
                            url: 'Service.asmx/deleteTblHelper',
                            method: 'POST',
                            contentType: 'application/json;charset-utf-8',
                            data: JSON.stringify({ tblNam: tableName, colNam: AttrName }),
                            dataType: 'json',
                            success: function (data) {
                                $('.myAlert.success').css('display', 'block');
                                $('#succMsg').html(' successfully delete ' + AttrName + ' Info from tblHelper');
                            },
                            error: function (err) {
                                //alert(err.responseText);
                                $('.myAlert.failed').css('display', 'block');
                                $('#failMsg').html('failed to delete ' + AttrName + ' Info from tblHelper table, please try again');
                            }
                        });

                    }

                },
                error: function (err) {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to delete this column from ' + tableName + ' table, please try again');
                }
            });

            fillTblFields();
            return false;
        }
        // update column data
        function UpdateAttr(tableName, AttrName) {
            //var btnClicked = $(this).html();
            //var testys = btnClicked.html();
            //alert(btnClicked);
            var newName = $(".tblattrs ." + AttrName).find(".colName").val();
            var tblNAME = $('.selectpicker option:selected').val();
            var btnClckUp = $(".tblattrs ." + AttrName).find(".DTDrop").find(":selected").val();
            var ftDrop = $(".tblattrs ." + AttrName).find(".FTDrop").find(":selected").val();
            var relTbl = $(".tblattrs ." + AttrName).find(".DBTblDrop").find(":selected").val();
            var relCol = $(".tblattrs ." + AttrName).find(".tblColDrop").find(":selected").val();
            var updateStr = " fromTbl = N'" + tableName + "', fromCol = N'" + AttrName + "'," + " refTbl = N'" + relTbl + "'," + " refCol = N'" + relCol + "'";
            var colWhere = "fromTbl";
            var searchkey = tableName + "' AND fromCol = N'" + AttrName;
            var txtStr = "N'" + tableName + "'," + "N'" + AttrName + "'," + "N'" + relTbl + "'," + "N'" + relCol + "'";

            //alert(btnClckUp);
            //update the database type and field type of column
            $.ajax({
                url: 'Service.asmx/updateCol',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: tableName, colName: AttrName, colDataType: btnClckUp, colFType: ftDrop, newName: newName }),
                dataType: 'json',
                success: function (data) {
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('Updated successfully');
                    fillTblFields();
                },
                error: function (err) {
                    //alert("Error");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to Update this Attribute, please try again');
                }
            });

            // update or create relation

            if (ftDrop == "select") {
                var upRel = $.ajax({
                    url: 'Service.asmx/UpdateExactRelation',
                    method: 'POST',
                    contentType: 'application/json;charset-utf-8',
                    data: JSON.stringify({ Tablename: tableName, UpdateStatement: updateStr, colWhere: colWhere, searchkey: searchkey, checky: AttrName, txt: txtStr, refCol: relCol, refTbl: relTbl }),
                    dataType: 'json',
                    success: function (data) {
                        $('.myAlert.success').css('display', 'block');
                        $('#succMsg').html('Updated Relation successfully');
                        //fillTblFields();
                    },
                    error: function (err) {
                        //alert("Error");
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed to Update this Attribute, please try again ');
                    }
                });
            }
            if (ftDrop != "select") {
                var upRel = $.ajax({
                    url: 'Service.asmx/deleteRel',
                    method: 'POST',
                    contentType: 'application/json;charset-utf-8',
                    data: JSON.stringify({ tblName: tblNAME, fType: ftDrop, colName: AttrName }),
                    dataType: 'json',
                    success: function (data) {
                        $('.myAlert.success').css('display', 'block');
                        $('#succMsg').html(' Updated successfully');
                        //alert(data.d);
                        //fillTblFields();
                    },
                    error: function (err) {
                        //alert("Error");
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html(' failed to Update, please try again');
                    }
                });
            }

            return false;
        }
        // submit insert column to table
        $("#btnInsertCol").click(function () {

            var tblNAME = $('.selectpicker option:selected').val();
            var ColName = $("#txtColNme").val();
            var ColAlias = $("#txtColAlias").val();
            var ColDT = $('#selectDT option:selected').val();
            var colPrimary = " ";
            var colNull = " NOT NULL ";
            var colIdentity = " ";
            var colViewOrder = $('#selectPV option:selected').val();
            var colEditable = $('#selectEdit option:selected').val();
            var colFieldType = $('#selectFT option:selected').val();
            var colFieldWidth = $("#txtColWidth").val();


            if ($('#Null').is(':checked')) {
                colNull = " ";
            }

            if ($('#Identity').is(':checked')) {
                colIdentity = " IDENTITY(1,1) ";
            }

            var str = ColDT + " " + colNull + " " + colIdentity + " ";
            var str2 = "'" + tblNAME + "','" + ColName + "','" + ColAlias + "','" + ColDT + "','" + colFieldWidth + "','" + colFieldType + "','" + colViewOrder + "','" + colEditable + "'";
            //alert(str2);
            $.ajax({
                url: 'Service.asmx/addCol2Tbl',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ tblName: tblNAME, colName: ColName, colData: str }),
                dataType: 'json',
                success: function (data) {
                    //alert("added Succefully!");
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('added successfully to the table');
                    fillTblFields();
                },
                error: function (err) {
                    //alert("error in adding Col! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to add new column to the table, please try again');
                }
            });
            $.ajax({
                url: 'Service.asmx/inserTblHelper',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ cols: 'tblName,colName,colAlias,DataType,colWidth,FieldType,priorityView,enabledView', txt: str2 }),
                dataType: 'json',
                success: function (data) {
                    //alert('added')
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('added successfully to the helper table');

                },
                error: function (err) {
                    //alert('error')
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to add data to the helper table, please try again');

                }
            });
            return false;
        });
        // save order of columns
        function saveOrder() {
            var NameOFTable = $(".selectpicker option:selected").text();
            var countys = $('ol.tblattrs li').length;
            var str = "";
            for (var i = 0; i < countys; i++) {
                if (i < countys - 1) {
                    if ($('ol.tblattrs li').eq(i).attr('class').toString() != 'ID') {
                        str += " ( N'" + NameOFTable + "',N'" + $('ol.tblattrs li').eq(i).attr('class').toString() + "'," + (i + 1) + " ) , ";
                    }
                }
                else {
                    if ($('ol.tblattrs li').eq(i).attr('class').toString() != 'ID') {
                        str += " ( N'" + NameOFTable + "',N'" + $('ol.tblattrs li').eq(i).attr('class').toString() + "'," + (i + 1) + " ) ";
                    }
                }
            }
            $.ajax({
                url: 'Service.asmx/colReorder',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ valueStr: str }),
                dataType: 'json',
                success: function (data) {
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('Order saved successfully');
                    fillTblFields();
                },
                error: function (err) {
                    //alert("Error");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed to save Order , please try again');
                }
            });
        }
        // Add Permessions In table Permessions
        function btnperms() {
            //alert('clicked');
            var ColSelected = new Array;
            var tblNAME = $(".selectpicker option:selected").text();
            var accessName = $("#selectPerm option:selected").text();
            var accessID = $("#selectPerm option:selected").val();
            //alert(accessID);
            //alert(tblNAME + "//" + accessName);

            $('ol.tblattrs li').each(function (i, elem) {
                var columnName = $(elem).attr('class');
                //alert(i+" : "+columnName);
                if ($('ol.tblattrs li .allowAtr' + i).is(':checked')) {
                    ColSelected.push(columnName);
                }
            });

            var columnsInDB = "AccessID,tblName,colName,allowed";
            //alert(ColSelected.length);
            if (accessName == "Access Name") {
                $('.myAlert.failed').css('display', 'block');
                $('#failMsg').html('please choose access level to add permeesion');
            }
            else {
                if (ColSelected.length > 0) {
                    for (var i = 0; i < ColSelected.length; i++) {
                        //alert(accessID + " _ " + accessName + " _ " + tblNAME + " _ " + ColSelected[i] + " _ YES");
                        var values = "" + accessID + ",'" + tblNAME + "','" + ColSelected[i] + "','YES'";
                        //alert(columnsInDB);
                        //alert(values);
                        $.ajax({
                            url: 'Service.asmx/inserGen',
                            method: 'POST',
                            contentType: 'application/json;charset-utf-8',
                            data: JSON.stringify({ tblName: "Permissions", cols: columnsInDB, txt: values }),
                            dataType: 'json',
                            success: function (data) {
                                //alert("added Succefully!");
                                //$('.myAlert.success').css('display', 'block');
                                //$('#succMsg').html('added successfully to the table');
                                //fillTblFields();
                            },
                            error: function (err) {
                                $('.myAlert.failed').css('display', 'block');
                                $('#failMsg').html('failed to add new column to the table, please try again');
                            }
                        });
                    }
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('added successfully to the table');
                }
                else {
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('no column selected to add permeesion to ' + accessName + ', please try again');
                }

            }

        }

        function AddAllPermToTable(tableName) {
            var columnsDB = "AccessID,tblName,colName,allowed";
            for (var i = 0; i < accessLevelsID.length; i++) {
                var vals = "" + accessLevelsID[i] + ",'" + tableName + "','ID','YES'";
                $.ajax({
                    url: 'Service.asmx/inserGen',
                    method: 'POST',
                    contentType: 'application/json;charset-utf-8',
                    data: JSON.stringify({ tblName: "Permissions", cols: columnsDB, txt: vals }),
                    dataType: 'json',
                    success: function (data) {

                    },
                    error: function (err) {
                        $('.myAlert.failed').css('display', 'block');
                        $('#failMsg').html('failed to add new column to the table, please try again');
                    }
                });
            }
        }


    </script>
    <script src="scripts/sorting.js"></script>
    <script>
        //make columns able to reorder
        $(document).ready(function () {
            $("ol.tblattrs").sortable();
        });
    </script>

</asp:Content>
