﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="InsertForm.aspx.cs" Inherits="CRUDAjax.InsertForm" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .cont {
            padding-top: 90px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }
        /*fake*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cont">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Insert New Profile</h4>
                                <p class="card-category">Complete your profile</p>
                            </div>
                            <div class="card-body">
                                <panel class="frm">
                                   
                                    
                           
                                </panel>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function () {
            var elements = ["Email address", "First Name", "Last Name", "Adress", "City", "Lamborghini Mercy, Yourchick she so thirsty, I am in that two seat Lambo."];
            var arr=[];
            for (var i = 0; i < elements.length; i++) {
                arr.push(elements[i]);
            }

            var firstPart = '<div class="row"><div class="col-md-12"><div class="form-group"><label class="bmd-label-floating">';

            var secondPart = '</label><input type="text" id="txtCity" class="form-control" /></div></div></div>';

            var lastPart = '<div class="row"><div class="col-md-12"><div class="form-group"><label>About You</label><div class="form-group"><label class="bmd-label-floating">Lamborghini Mercy, Yourchick she so thirsty, I am in that two seat Lambo.</label><textarea class="form-control" id="txtAbout" rows="5"></textarea></div></div></div></div>';

            var buttton = '<button type="submit" id="btnInsert" class="btn btn-primary pull-right">Insert</button><div class="clearfix"></div>';


            for (var i = 0; i < elements.length - 1 ; i++) {
                $('.frm').append(firstPart + arr[i] + secondPart);
            }
            $('.frm').append(lastPart + buttton);
        });
    </script>
</asp:Content>
