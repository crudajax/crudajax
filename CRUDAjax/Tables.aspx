﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="Tables.aspx.cs" Inherits="CRUDAjax.Tables" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .cont {
            margin-top: 90px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }
        body .myAlert {
            width: 80%;
            z-index: 8000;
            margin: auto;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cont">
        <div class="alert alert-success alert-dismissible fade show myAlert success" role="alert">
            <strong>Good News!</strong> <span id="succMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger alert-dismissible fade show myAlert failed" role="alert">
            <strong>Sorry!</strong> <span id="failMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title ">Simple Table</h4>
                                <p class="card-category">Here is a subtitle for this table</p>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="dataTable" class="table">
                                        <thead class=" text-primary">
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>E-mail</th>
                                                <th>Address</th>
                                                <th>City</th>
                                                <th>About</th>
                                                <th class="text-center" colspan="2">Edit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(getEmps());
        function getEmps() {
            $('#dataTable tbody tr').remove();
            //var id = 1;
            $.ajax({
                url: 'Service.asmx/selectEmps',
                method: 'post',
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    var KeyD = jqueryXML.find("Table").length;

                    for (var i = 0; i < parseInt(KeyD) ; i++) {
                        $('#dataTable').append(
                             $('<tr>')
                                .append($('<td>').append(jqueryXML.find('fName').eq(i).text()))
                                .append($('<td>').append(jqueryXML.find('lName').eq(i).text()))
                                .append($('<td>').append(jqueryXML.find('email').eq(i).text()))
                                .append($('<td>').append(jqueryXML.find('address').eq(i).text()))
                                .append($('<td>').append(jqueryXML.find('city').eq(i).text()))
                                .append($('<td>').append(jqueryXML.find('about').eq(i).text()))
                                .append($('<td>').append($('<a>').attr('href', 'Update.aspx?ID=' + jqueryXML.find('ID').eq(i).text()).append($('<i>').attr('class', 'material-icons').text('edit'))))
                                .append($('<td>').append($('<div>').attr('ids', jqueryXML.find('ID').eq(i).text()).attr('class', 'btnDelete').attr('onclick', 'deleteEmp(' + parseInt(jqueryXML.find("ID").eq(i).text()) + ');').append($('<i>').attr('class', 'fa fa-trash').attr('style', "font-size: 24px; color: red;     cursor: pointer;"))))
                                .append($('<input>').val(jqueryXML.find('ID').eq(i).text()).attr('type', 'hidden').addClass('idHidden'))
                            );
                    }

                },
                error: function (err) {
                    //alert("error in retrieving employee! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in retrieving employee, please try again');
                }
            });
        }
    </script>
    <script>
        function deleteEmp(IDDelete) {

            $.ajax({
                url: 'Service.asmx/deleteEmp',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ idDel: parseInt(IDDelete) }),
                dataType: 'json',
                success: function (data) {
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('Successfully Deleted');
                    //alert("Deleted!");
                    //response(data.d);

                },
                error: function (err) {
                    //alert("error in Deleting employee! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in Deleting employee, please try again');
                    
                }
            });
            //alert(date);
            var form = $("#form1");
            var post_url = form.attr('action');
            var post_data = getEmps();
            $.ajax({
                type: 'POST',
                url: post_url,
                data: post_data,
                success: function () {
                   
                }
            });
            return false;
        }

    </script>

</asp:Content>
