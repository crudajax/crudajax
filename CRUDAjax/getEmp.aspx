﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="getEmp.aspx.cs" Inherits="CRUDAjax.getEmp" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .cont {
            padding-top: 90px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }

         body .myAlert {
            width: 80%;
            z-index: 8000;
            margin: auto;
            display: none;
        }
        /*fake*/
    </style>
    <script>
        $(document).ready(function () {
            $.ajax({
                url: 'Service.asmx/getEmpById',
                method: 'post',
                data: { ID: 1 },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    $(".FName").text(jqueryXML.find('fName').text());
                    $(".LName").text(jqueryXML.find('lName').text());
                },
                error: function (err) {
                    //alert("error in adding employee! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in adding employee, please try again');
                }
            });
        });
        $(document).ready(function () {
            $.ajax({
                url: 'Service.asmx/getEmp',
                method: 'post',
                data: { },
                dataType: 'xml',
                success: function (data) {
                    var jqueryXML = $(data);
                    //alert(jqueryXML.find('NewDataSet').text())
                    //alert(jqueryXML.find('Table').eq(2).text())
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html(jqueryXML.find('NewDataSet').text()+"   "+jqueryXML.find('Table').eq(2).text());

                },
                error: function (err) {
                    //alert("error in adding employee! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in adding employee, please try again');
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cont">
        <div class="alert alert-success alert-dismissible fade show myAlert success" role="alert">
            <strong>Good News!</strong> <span id="succMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger alert-dismissible fade show myAlert failed" role="alert">
            <strong>Sorry!</strong> <span id="failMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>

        <table class="table">
            <tr>
                <td>first Name</td>
                <td>
                    <input type="text" id="txtFName" /></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td>
                    <input type="text" id="txtLName" /></td>
            </tr>
        </table>
        <label class="FName"></label>
        <label class="LName"></label>

    </div>
</asp:Content>
