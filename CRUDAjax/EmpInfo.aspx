﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="EmpInfo.aspx.cs" Inherits="CRUDAjax.EmpInfo" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cpTitle" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .cont {
            padding-top: 90px;
            min-height: 550px;
            overflow: auto;
            overflow-y: auto;
            clear: both;
        }

         body .myAlert {
            width: 80%;
            z-index: 8000;
            margin: auto;
            display: none;
        }

        /*fake*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cont">
        <div class="alert alert-success alert-dismissible fade show myAlert success" role="alert">
            <strong>Good News!</strong> <span id="succMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger alert-dismissible fade show myAlert failed" role="alert">
            <strong>Sorry!</strong> <span id="failMsg">You should check in on some of those fields below.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span class="hideAlert" aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Insert New Profile</h4>
                                <p class="card-category">Complete your profile</p>
                            </div>
                            <div class="card-body">
                                <panel>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Email address</label>
                                                <input type="email" id="txtMail" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Fist Name</label>
                                                <input type="text" id="txtFname" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Last Name</label>
                                                <input type="text" id="txtLname" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Adress</label>
                                                <input type="text" id="txtAddress" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">City</label>
                                                <input type="text" id="txtCity" class="form-control" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>About You</label>
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</label>
                                                    <textarea class="form-control" id="txtAbout" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" id="btnInsert" class="btn btn-primary pull-right">Insert</button>
                                    <div class="clearfix"></div>
                                </panel>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-profile">
                            <div class="card-avatar">
                                <a href="#pablo">
                                    <img class="img" src="assets/img/faces/marc.jpg" />
                                </a>
                            </div>
                            <div class="card-body">
                                <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                                <h4 class="card-title">Alec Thompson</h4>
                                <p class="card-description">
                                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                                </p>
                                <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#btnInsert").click(function () {
            //alert("clicked");
            var mail = "'" + $("#txtMail").val() + "'";
            var fName = "'" + $("#txtFname").val() + "'";
            var lName = "'" + $("#txtLname").val() + "'";
            var address = "'" + $("#txtAddress").val() + "'";
            var city = "'" + $("#txtCity").val() + "'";
            var about = "'" + $("#txtAbout").val() + "'";
            var str = mail + "," + fName + "," + lName + "," + address + "," + city + "," + about;
            //alert(str);
            $.ajax({
                url: 'Service.asmx/inserEmp',
                method: 'POST',
                contentType: 'application/json;charset-utf-8',
                data: JSON.stringify({ txt: str }),
                dataType: 'json',
                success: function (data) {
                    //alert(data.d);
                    var newUrl = "http://localhost:12395/Tables.aspx";
                    //alert("added Succefully!");
                    $('.myAlert.success').css('display', 'block');
                    $('#succMsg').html('added Succefully');
                    $(location).attr('href', newUrl);
                    

                },
                error: function (err) {
                    //alert("error in adding employee! Sorry");
                    $('.myAlert.failed').css('display', 'block');
                    $('#failMsg').html('failed in adding employee, please try again');
                }
            });
            return false;
        });

    </script>

</asp:Content>
